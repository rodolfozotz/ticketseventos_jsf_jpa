package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioDAO.modification;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.EnderecoBC;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EnderecoEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioBC.TesteEnderecoBC;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.util.Atualizador;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.util.Verificador;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

public class TesteEnderecoDAOModification extends TesteEnderecoBC {
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TesteEnderecoDAOModification(EnderecoEnum endereco) {
		super(endereco);
	}

	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Metodo responsavel por realizar um INSERT na base de Dados
	 */
	@Test
	@Override
	public void criar(){
		//Realiza o INSERT na chamada do metodo para o BC
		//DICA: Caso não esteja rodando aqui, veja se voce esta chamando a classe DAO a partir da BC
		super.criar();
		
		//Verifica se o id eh maior que zero
		Assert.assertTrue(endereco.getId() > 0);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Verificador.verificar(EnderecoBC.getInstance().findById(endereco.getId()), endereco);
	}
	
	/**
	 * Metodo responsavel por atualizar um endereco completo
	 */
	@Test
	public void updateEnderecoCompleto(){
		//Realiza o INSERT na chamada do metodo para o BC
		criar();
		
		//Atualiza Atributos
		Atualizador.atualizar(endereco, "E-");
		
		//Atualiza o objeto no BD
		EnderecoBC.getInstance().update(endereco);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Verificador.verificar(EnderecoBC.getInstance().findById(endereco.getId()), endereco);
		
		//Remove o objeto do BD - Para nao dar problemas no teste de queries
		EnderecoBC.getInstance().delete(endereco);
	}
	
	/**
	 * Metodo responsavel por deletar um endereco completo
	 */
	@Test
	public void deletarEnderecoCompleto(){
		//Realiza o INSERT na chamada do metodo para o BC
		criar();

		//Remove o objeto do BD
		EnderecoBC.getInstance().delete(endereco);
		
		//Verifica se o objeto deixou de existir no BD
		Assert.assertNull(EnderecoBC.getInstance().findById(endereco.getId()));
	}
}
