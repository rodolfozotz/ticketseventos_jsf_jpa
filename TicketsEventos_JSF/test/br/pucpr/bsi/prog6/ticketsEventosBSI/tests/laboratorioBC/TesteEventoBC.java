package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioBC;

import java.util.Date;

import org.junit.Test;

import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.EventoBCFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.exception.BSIException;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Filme;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Ingresso;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Peca;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Show;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioModel.TesteEventoModel;
import br.pucpr.bsi.prog6.ticketsEventosBSI.utils.DateUtils;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

@SuppressWarnings("unchecked")
public class TesteEventoBC extends TesteEventoModel{
	
	///////////////////////////////////////////
	// CONSTRUTORES
	///////////////////////////////////////////
	
	public TesteEventoBC(EventoEnum evento) {
		super(evento);
	}
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Metodo responsavel por criar um evento completo
	 */
	@Test
	@Override
	public void criar(){
		super.criar();
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).insert(evento);
	}
	
	/**
	 * Valida o evento nulo
	 */
	@Test(expected = BSIException.class)
	public void validarEventoNulo(){
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).insert(null);
	}
	
	/**
	 * Valida o evento com ambiente nulo
	 */
	@Test(expected = BSIException.class)
	public void validarEventoAmbienteNulo(){
		evento.setAmbiente(null);
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).insert(evento);
	}
	
	/**
	 * Valida o evento com lista artistas nulo
	 */
	@Test(expected = BSIException.class)
	public void validarEventoListaArtistaNulo(){
		evento.setArtistas(null);
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).insert(evento);
	}
	
	/**
	 * Valida o evento com artista nulo
	 */
	@Test(expected = BSIException.class)
	public void validarEventoArtistaNulo(){
		evento.getArtistas().clear();
		evento.getArtistas().add(null);
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).insert(evento);
	}
	
	/**
	 * Valida o evento com diretor nulo
	 */
	@Test(expected = BSIException.class)
	public void validarEventoDiretorNulo(){
		evento.setDiretor(null);
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).insert(evento);
	}
	
	/**
	 * Valida o evento com o nome preenchido com espacos em branco
	 */
	@Test(expected = BSIException.class)
	public void validarEventoEspacosBranco(){
		evento.setNome("                              ");
		evento.setDescricao("                         ");
		
		switch(evento.getTipoEventoEnum()){
		case FILME:
			((Filme)evento).setGenero("                            ");
			break;
		case PECA:
			((Peca)evento).setGenero("                            ");
			((Peca)evento).setCompanhia("                            ");
			break;
		case SHOW:
			((Show)evento).setEstilo("                            ");
		}
		
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).insert(evento);
	}	
	
	/**
	 * Valida o evento sem nome
	 */
	@Test(expected = BSIException.class)
	public void validarEventoSemNome(){
		evento.setNome(null);
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).insert(evento);
	}	
	
	/**
	 * Valida o evento sem descricao
	 */
	@Test(expected = BSIException.class)
	public void validarEventoSemDescricao(){
		evento.setDescricao(null);
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).insert(evento);
	}
	
	/**
	 * Valida o evento sem censura
	 */
	@Test(expected = BSIException.class)
	public void validarEventoSemCensura(){
		evento.setCensura(null);
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).insert(evento);
	}
	
	/**
	 * Valida o evento com censura negativa
	 */
	@Test(expected = BSIException.class)
	public void validarEventoComCensuraNegativa(){
		evento.setCensura(-20);
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).insert(evento);
	}
	
	/**
	 * Valida o evento sem data evento
	 */
	@Test(expected = BSIException.class)
	public void validarEventoSemDataEvento(){
		evento.setDataEvento(null);
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).insert(evento);
	}
	
	/**
	 * Valida o evento com data evento 1 ano antes da data atual
	 */
	@Test(expected = BSIException.class)
	public void validarEventoComDataEventoAntesDataAtual(){
		evento.setDataEvento(DateUtils.datePlusDays(new Date(), -365));
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).insert(evento);
	}
	
	/**
	 * Valida o evento com ingressos diferentes
	 */
	@Test(expected = BSIException.class)
	public void validarEventoIngressosDiferentes(){
		evento.setIngressos(null);
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).insert(evento);
	}
	
	/**
	 * Valida o evento com ingresso sem setor
	 */
	@Test(expected = BSIException.class)
	public void validarEventoIngressoSemSetor(){
		for(Ingresso ingresso : evento.getIngressos()){
			ingresso.setSetor(null);
		}
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).insert(evento);
	}
	
	/**
	 * Valida o evento com ingresso sem evento
	 */
	@Test(expected = BSIException.class)
	public void validarEventoIngressoSemEvento(){
		for(Ingresso ingresso : evento.getIngressos()){
			ingresso.setEvento(null);
		}
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).insert(evento);
	}
	
	/**
	 * Valida o evento sem companhia
	 */
	@Test(expected = BSIException.class)
	public void validarEventoSemCompanhia(){
		switch(evento.getTipoEventoEnum()){
		case PECA:
			((Peca)evento).setCompanhia(null);
			break;
		case FILME:
		case SHOW:
			throw new BSIException("N�o � poss�vel testar");
		}
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).insert(evento);
	}
	
	/**
	 * Valida o evento sem estilo
	 */
	@Test(expected = BSIException.class)
	public void validarEventoSemEstilo(){
		switch(evento.getTipoEventoEnum()){
		case SHOW:
			((Show)evento).setEstilo(null);
			break;
		case PECA:
		case FILME:
			throw new BSIException("N�o � poss�vel testar");
		}
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).insert(evento);
	}
	
	/**
	 * Valida o evento sem genero
	 */
	@Test(expected = BSIException.class)
	public void validarEventoSemGenero(){
		switch(evento.getTipoEventoEnum()){
		case FILME:
			((Filme)evento).setGenero(null);	
			break;
		case PECA:
			((Peca)evento).setGenero(null);
			break;
		case SHOW:
			throw new BSIException("N�o � poss�vel testar");
		}
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).insert(evento);
	}
}
