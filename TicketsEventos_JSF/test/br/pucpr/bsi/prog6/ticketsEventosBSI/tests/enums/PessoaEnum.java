package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums;

import java.util.Date;
import java.util.Random;

import br.pucpr.bsi.prog6.ticketsEventosBSI.utils.DateUtils;

public enum PessoaEnum {
	
	CLIENTE_01('C', "Cliente 01", "cliente01@clientes.com.br", "11111111", "09/03/1997", 
			"senhaPass01", "cliente01", "3456345634563456", "12121212", EnderecoEnum.getEndereco()),
			
	CLIENTE_02('C', "Cliente 02", "cliente02@clientes.com.br", "22222222", "07/05/2000", 
			"senhaPass02", "cliente02", "1234567812345678", "56565656", EnderecoEnum.getEndereco()),
			
	CLIENTE_03('C', "Cliente 03", "cliente03@clientes.com.br", "33333333", "20/07/1954", 
			"senhaPass03", "cliente03", "7890789078907890", "34343434", EnderecoEnum.getEndereco()),
			
	CLIENTE_04('C', "Cliente 04", "cliente04@clientes.com.br", "44444444", "25/01/1985", 
			"senhaPass04", "cliente04", "1010101010101010", "45454545", EnderecoEnum.getEndereco()),
			
	CLIENTE_05('C', "Cliente 05", "cliente05@clientes.com.br", "55555555", "09/02/1984", 
			"senhaPass05", "cliente05", "4567456745674567", "67676767", EnderecoEnum.getEndereco()),
			
	CLIENTE_06('C', "Cliente 06", "cliente06@clientes.com.br", "66666666", "14/03/1987",
			"senhaPass06", "cliente06", "1234123412341234", "89898989", EnderecoEnum.getEndereco()),
	
	
	FUNCIONARIO_01('F', "Funcionario 01", "funcionario01@funcionarios.com.br", "f11111111", "09/03/1987", 
			"senhaPass01", "funcionario01", "f345", "12121212", EnderecoEnum.getEndereco()),
			
	FUNCIONARIO_02('F', "Funcionario 02", "funcionario02@funcionarios.com.br", "f22222222", "07/05/1960", 
			"senhaPass02", "funcionario02", "f123", "56565656", EnderecoEnum.getEndereco()),
			
	FUNCIONARIO_03('F', "Funcionario 03", "funcionario03@funcionarios.com.br", "f33333333", "20/07/1984", 
			"senhaPass03", "funcionario03", "f789", "34343434", EnderecoEnum.getEndereco()),
			
	FUNCIONARIO_04('F', "Funcionario 04", "funcionario04@funcionarios.com.br", "f44444444", "25/01/1985", 
			"senhaPass04", "funcionario04", "f101", "45454545", EnderecoEnum.getEndereco()),
			
	FUNCIONARIO_05('F', "Funcionario 05", "funcionario05@funcionarios.com.br", "f55555555", "09/02/1974", 
			"senhaPass05", "funcionario05", "f456", "67676767", EnderecoEnum.getEndereco()),
			
	FUNCIONARIO_06('F', "Funcionario 06", "funcionario06@funcionarios.com.br", "f66666666", "14/03/1967",
			"senhaPass06", "funcionario06", "f234", "89898989", EnderecoEnum.getEndereco());
	
	
	private char tipo;
	private String nome, email, telefone, dataNascimento, senha,  usuario, numeroCartao, documento, codigo, contaCorrente;
	private EnderecoEnum enderecoEnum;
	
	private PessoaEnum(char tipo, String nome, String email, String telefone, String dataNascimento,  
				String senha, String usuario, String cartaoOuCodigo, String documentoOuContaCorrente, EnderecoEnum enderecoEnum) {
		this.tipo = tipo;
		this.dataNascimento = dataNascimento;
		this.email = email;
		this.nome = nome;
		this.senha = senha;
		this.telefone = telefone;
		this.usuario = usuario;
		this.enderecoEnum = enderecoEnum;
		
		if(tipo == 'C'){
			this.numeroCartao = cartaoOuCodigo;
			this.documento = documentoOuContaCorrente;
		} else {
			this.codigo = cartaoOuCodigo;
			this.contaCorrente = documentoOuContaCorrente;
		}
	}
	
	public char getTipo() {
		return tipo;
	}
	
	public Date getDataNascimento() {
		return DateUtils.converterDate(dataNascimento);
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getNome() {
		return nome;
	}
	
	public String getSenha() {
		return senha;
	}
	
	public String getTelefone() {
		return telefone;
	}
	
	public String getUsuario() {
		return usuario;
	}
	
	public String getNumeroCartao() {
		return numeroCartao;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public String getContaCorrente() {
		return contaCorrente;
	}
	
	public String getDocumento() {
		return documento;
	}
	
	public EnderecoEnum getEnderecoEnum() {
		return enderecoEnum;
	}
	
	public static PessoaEnum getPessoa(){
		Random generator = new Random();
		PessoaEnum[] array = values();
		int length = array.length;
		return array[generator.nextInt(length)];
	}
	
	public static PessoaEnum[] getClientes(){
		PessoaEnum[] array = {CLIENTE_01, CLIENTE_02, CLIENTE_03, CLIENTE_04, CLIENTE_05, CLIENTE_06};
		return array;
	}
	
}
