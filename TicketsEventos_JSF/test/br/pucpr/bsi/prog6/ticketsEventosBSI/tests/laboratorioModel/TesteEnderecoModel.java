package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioModel;

import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EnderecoEnum.*;

import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EnderecoEnum;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

@RunWith(Parameterized.class)
public class TesteEnderecoModel {

	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////		
	protected EnderecoEnum enderecoEnum;
	protected Endereco endereco;

	@SuppressWarnings("rawtypes")
	@Parameterized.Parameters
	public static Collection listaObjetos() {
		return Arrays.asList(new Object[][] {
				{ END_BRASILIA },
				{ END_BELO_HORIZONTE },
				{ END_CURITIBA },
				{ END_SAO_PAULO },
				{ END_PORTO_ALEGRE },
				{ END_RIO_JANEIRO }
		});
	}	
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TesteEnderecoModel(EnderecoEnum enderecoEnum) {
		this.enderecoEnum = enderecoEnum;
	}
	
	
	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////	
	
	/**
	 * Metodo para criar um novo Endereco
	 * @return
	 */
	public static Endereco novoEndereco(EnderecoEnum enderecoEnum){
		Endereco endereco = new Endereco();
		endereco.setBairro(enderecoEnum.getBairro());
		endereco.setCidade(enderecoEnum.getCidade());
		endereco.setComplemento(enderecoEnum.getComplemento());
		endereco.setEstado(enderecoEnum.getEstado());
		endereco.setNumero(enderecoEnum.getNumero());
		endereco.setPais(enderecoEnum.getPais());
		endereco.setRua(enderecoEnum.getRua());
		return endereco;
	}
	
	
	@Before
	public void criaEndereco(){
		this.endereco = novoEndereco(this.enderecoEnum);
	}
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////
	
	@Test
	public void criar() {
		//Verifica se nao eh nulo
		Assert.assertNotNull(endereco);
		
		//Verifica se os atributos estao preenchidos
		Assert.assertTrue(StringUtils.isNotBlank(endereco.getBairro()));
		Assert.assertTrue(StringUtils.isNotBlank(endereco.getCidade()));
		Assert.assertTrue(StringUtils.isNotBlank(endereco.getComplemento()));
		Assert.assertTrue(StringUtils.isNotBlank(endereco.getEstado()));
		Assert.assertTrue(StringUtils.isNotBlank(endereco.getPais()));
		Assert.assertTrue(StringUtils.isNotBlank(endereco.getRua()));
		Assert.assertNotNull(endereco.getNumero());
		
		//Alteracao dos atributos
		EnderecoEnum enderecoEnumTemp = EnderecoEnum.getEndereco();
		endereco.setBairro(enderecoEnumTemp.getBairro());
		endereco.setCidade(enderecoEnumTemp.getCidade());
		endereco.setComplemento(enderecoEnumTemp.getComplemento());
		endereco.setEstado(enderecoEnumTemp.getEstado());
		endereco.setNumero(enderecoEnumTemp.getNumero());
		endereco.setPais(enderecoEnumTemp.getPais());
		endereco.setRua(enderecoEnumTemp.getRua());
		
		//Verifica se alteracao dos atributos esta sendo realizada
		Assert.assertEquals(endereco.getBairro(), enderecoEnumTemp.getBairro());
		Assert.assertEquals(endereco.getCidade(), enderecoEnumTemp.getCidade());
		Assert.assertEquals(endereco.getComplemento(), enderecoEnumTemp.getComplemento());
		Assert.assertEquals(endereco.getEstado(), enderecoEnumTemp.getEstado());
		Assert.assertEquals(endereco.getNumero(), enderecoEnumTemp.getNumero(), 0);
		Assert.assertEquals(endereco.getPais(), enderecoEnumTemp.getPais());
		Assert.assertEquals(endereco.getRua(), enderecoEnumTemp.getRua());
		
		//Volta os atributos, para ficar compativel com outros testes		
		endereco.setBairro(enderecoEnum.getBairro());
		endereco.setCidade(enderecoEnum.getCidade());
		endereco.setComplemento(enderecoEnum.getComplemento());
		endereco.setEstado(enderecoEnum.getEstado());
		endereco.setNumero(enderecoEnum.getNumero());
		endereco.setPais(enderecoEnum.getPais());
		endereco.setRua(enderecoEnum.getRua());
	}
}
