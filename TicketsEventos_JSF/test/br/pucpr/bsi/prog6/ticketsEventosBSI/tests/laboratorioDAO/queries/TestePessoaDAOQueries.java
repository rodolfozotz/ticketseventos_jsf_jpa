package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioDAO.queries;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.PessoaBCFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.exception.BSIException;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Pessoa;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.PessoaFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.enums.TipoPessoaEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EnderecoEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.PessoaEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.util.Verificador;
import br.pucpr.bsi.prog6.ticketsEventosBSI.utils.DateUtils;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * IMPORTANTE: Para rodar esses teste, limpar a base do Banco de Dados antes e rodar os testes da camada BC.
 * 
 * @author Mauda
 * 
 */

@SuppressWarnings("unchecked")
public class TestePessoaDAOQueries {
	
	///////////////////////////////////////////////////////////
	// TESTE DOS METODOS findAll E findById 
	///////////////////////////////////////////////////////////
	
	@Test
	public void testFindAlleFindById(){
		List<Pessoa> listaBD = PessoaBCFactory.getInstance(TipoPessoaEnum.CLIENTE).findAll();
		listaBD.addAll(PessoaBCFactory.getInstance(TipoPessoaEnum.FUNCIONARIO).findAll());
		
		//Verifica se a quantidade de instancias a mesma dos testes de populate
		Assert.assertEquals(listaBD.size(), PessoaEnum.values().length);
		
		for (Pessoa pessoaFindAll : listaBD) {
			Pessoa pessoaFindId = (Pessoa) PessoaBCFactory.getInstance(pessoaFindAll.getTipoPessoaEnum()).findById(pessoaFindAll.getId());
			Verificador.verificar(pessoaFindAll, pessoaFindId);
		}
		
		//buscando um id inexistente
		Pessoa pessoaIdInvalido = (Pessoa) PessoaBCFactory.getInstance(TipoPessoaEnum.CLIENTE).findById(100000L);
		Assert.assertNull(pessoaIdInvalido);
	}
	
	///////////////////////////////////////////////////////////
	// TESTES DO METODO findByFilter 
	///////////////////////////////////////////////////////////	
		
	@Test(expected = BSIException.class)
	public void testFindByFilterNulo(){
		PessoaBCFactory.getInstance(TipoPessoaEnum.FUNCIONARIO).findByFilter(null);
	}	
	
	@Test
	public void testFindByFilterDataNascimentoPessoa(){
		for (PessoaEnum pessoaEnum : PessoaEnum.values()) {
			Pessoa filter = PessoaFactory.getInstance(TipoPessoaEnum.getBy(pessoaEnum.getTipo()), new Endereco());
			filter.setDataNascimento(pessoaEnum.getDataNascimento());
			List<Pessoa> pessoaFilter = PessoaBCFactory.getInstance(filter.getTipoPessoaEnum()).findByFilter(filter);
			
			//verifica se o tamanho do filtro eh 1
			Assert.assertEquals(pessoaFilter.size(), 1);
			
			for (Pessoa pessoa : pessoaFilter) {
				Assert.assertEquals(DateUtils.minimizeDate(pessoa.getDataNascimento()),
						DateUtils.minimizeDate(pessoaEnum.getDataNascimento()));				
			}
		}
	}
	
	@Test
	public void testFindByFilterEmailPessoa(){
		for (PessoaEnum pessoaEnum : PessoaEnum.values()) {
			Pessoa filter = PessoaFactory.getInstance(TipoPessoaEnum.getBy(pessoaEnum.getTipo()), new Endereco());
			filter.setEmail(pessoaEnum.getEmail());
			List<Pessoa> pessoaFilter = PessoaBCFactory.getInstance(filter.getTipoPessoaEnum()).findByFilter(filter);
			
			//verifica se o tamanho do filtro eh 1
			Assert.assertEquals(pessoaFilter.size(), 1);
			
			for (Pessoa pessoa : pessoaFilter) {
				Assert.assertEquals(pessoa.getEmail(), pessoaEnum.getEmail());				
			}
		}
	}	
	
	@Test
	public void testFindByFilterNomePessoa(){
		for (PessoaEnum pessoaEnum : PessoaEnum.values()) {
			Pessoa filter = PessoaFactory.getInstance(TipoPessoaEnum.getBy(pessoaEnum.getTipo()), new Endereco());
			filter.setNome(pessoaEnum.getNome());
			List<Pessoa> pessoaFilter = PessoaBCFactory.getInstance(filter.getTipoPessoaEnum()).findByFilter(filter);
			
			//verifica se o tamanho do filtro eh 1
			Assert.assertEquals(pessoaFilter.size(), 1);
			
			for (Pessoa pessoa : pessoaFilter) {
				Assert.assertEquals(pessoa.getNome(), pessoaEnum.getNome());				
			}
		}
	}	
	
	@Test
	public void testFindByFilterTelefonePessoa(){
		for (PessoaEnum pessoaEnum : PessoaEnum.values()) {
			Pessoa filter = PessoaFactory.getInstance(TipoPessoaEnum.getBy(pessoaEnum.getTipo()), new Endereco());
			filter.setTelefone(pessoaEnum.getTelefone());
			List<Pessoa> pessoaFilter = PessoaBCFactory.getInstance(filter.getTipoPessoaEnum()).findByFilter(filter);
			
			//verifica se o tamanho do filtro eh 1
			Assert.assertEquals(pessoaFilter.size(), 1);
			
			for (Pessoa pessoa : pessoaFilter) {
				Assert.assertEquals(pessoa.getTelefone(), pessoaEnum.getTelefone());				
			}
		}
	}	
	
	@Test
	public void testFindByFilterUsuarioPessoa(){
		for (PessoaEnum pessoaEnum : PessoaEnum.values()) {
			Pessoa filter = PessoaFactory.getInstance(TipoPessoaEnum.getBy(pessoaEnum.getTipo()), new Endereco());
			filter.setUsuario(pessoaEnum.getUsuario());
			List<Pessoa> pessoaFilter = PessoaBCFactory.getInstance(filter.getTipoPessoaEnum()).findByFilter(filter);
			
			//verifica se o tamanho do filtro eh 1
			Assert.assertEquals(pessoaFilter.size(), 1);
			
			for (Pessoa pessoa : pessoaFilter) {
				Assert.assertEquals(pessoa.getUsuario(), pessoaEnum.getUsuario());				
			}
		}
	}	
	
	///////////////////////////////////////////////////////////
	// DADOS DO ENDERECO
	///////////////////////////////////////////////////////////
	
	@Test
	public void testFindByFilterRuaEndereco(){
		Pessoa filter = PessoaFactory.getInstance(TipoPessoaEnum.CLIENTE, new Endereco());
		for (EnderecoEnum enderecoEnum : EnderecoEnum.values()) {
			filter.getEndereco().setRua(enderecoEnum.getRua());
			List<Pessoa> pessoaFilter = PessoaBCFactory.getInstance(filter.getTipoPessoaEnum()).findByFilter(filter);
			
			for (Pessoa pessoa : pessoaFilter) {
				Assert.assertEquals(pessoa.getEndereco().getRua(), enderecoEnum.getRua());				
			}
		}
	}	
	
	@Test
	public void testFindByFilterNumeroEndereco(){
		Pessoa filter = PessoaFactory.getInstance(TipoPessoaEnum.CLIENTE, new Endereco());
		for (EnderecoEnum enderecoEnum : EnderecoEnum.values()) {
			filter.getEndereco().setNumero(enderecoEnum.getNumero());
			List<Pessoa> pessoaFilter = PessoaBCFactory.getInstance(filter.getTipoPessoaEnum()).findByFilter(filter);
			
			for (Pessoa pessoa : pessoaFilter) {
				Assert.assertEquals(pessoa.getEndereco().getNumero(), enderecoEnum.getNumero(), 0);				
			}
		}
	}	
	
	@Test
	public void testFindByFilterComplementoEndereco(){
		Pessoa filter = PessoaFactory.getInstance(TipoPessoaEnum.CLIENTE, new Endereco());
		for (EnderecoEnum enderecoEnum : EnderecoEnum.values()) {
			filter.getEndereco().setComplemento(enderecoEnum.getComplemento());
			List<Pessoa> pessoaFilter = PessoaBCFactory.getInstance(filter.getTipoPessoaEnum()).findByFilter(filter);
			
			for (Pessoa pessoa : pessoaFilter) {
				Assert.assertEquals(pessoa.getEndereco().getComplemento(), enderecoEnum.getComplemento());				
			}
		}
	}	
	
	@Test
	public void testFindByFilterBairroEndereco(){
		Pessoa filter = PessoaFactory.getInstance(TipoPessoaEnum.CLIENTE, new Endereco());
		for (EnderecoEnum enderecoEnum : EnderecoEnum.values()) {
			filter.getEndereco().setBairro(enderecoEnum.getBairro());
			List<Pessoa> pessoaFilter = PessoaBCFactory.getInstance(filter.getTipoPessoaEnum()).findByFilter(filter);
			
			for (Pessoa pessoa : pessoaFilter) {
				Assert.assertEquals(pessoa.getEndereco().getBairro(), enderecoEnum.getBairro());				
			}
		}
	}
	
	@Test
	public void testFindByFilterCidadeEndereco(){
		Pessoa filter = PessoaFactory.getInstance(TipoPessoaEnum.CLIENTE, new Endereco());
		for (EnderecoEnum enderecoEnum : EnderecoEnum.values()) {
			filter.getEndereco().setCidade(enderecoEnum.getCidade());
			List<Pessoa> pessoaFilter = PessoaBCFactory.getInstance(filter.getTipoPessoaEnum()).findByFilter(filter);
			
			for (Pessoa pessoa : pessoaFilter) {
				Assert.assertEquals(pessoa.getEndereco().getCidade(), enderecoEnum.getCidade());				
			}
		}
	}
	
	@Test
	public void testFindByFilterEstadoEndereco(){
		Pessoa filter = PessoaFactory.getInstance(TipoPessoaEnum.CLIENTE, new Endereco());
		for (EnderecoEnum enderecoEnum : EnderecoEnum.values()) {
			filter.getEndereco().setEstado(enderecoEnum.getEstado());
			List<Pessoa> pessoaFilter = PessoaBCFactory.getInstance(filter.getTipoPessoaEnum()).findByFilter(filter);
			
			for (Pessoa pessoa : pessoaFilter) {
				Assert.assertEquals(pessoa.getEndereco().getEstado(), enderecoEnum.getEstado());				
			}
		}
	}
	
	@Test
	public void testFindByFilterPaisEndereco(){
		Pessoa filter = PessoaFactory.getInstance(TipoPessoaEnum.CLIENTE, new Endereco());
		for (EnderecoEnum enderecoEnum : EnderecoEnum.values()) {
			filter.getEndereco().setPais(enderecoEnum.getPais());
			List<Pessoa> pessoaFilter = PessoaBCFactory.getInstance(filter.getTipoPessoaEnum()).findByFilter(filter);
			
			for (Pessoa pessoa : pessoaFilter) {
				Assert.assertEquals(pessoa.getEndereco().getPais(), enderecoEnum.getPais());				
			}
		}
	}
	
}
