package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.util;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Ambiente;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Cliente;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Diretor;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Evento;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Filme;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Funcionario;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Ingresso;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Peca;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Pessoa;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Show;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Venda;
import br.pucpr.bsi.prog6.ticketsEventosBSI.utils.DateUtils;

/**
 * Classe responsavel por atualizar informacoes para update e comparacoes
 * @author Mauda
 *
 */
public class Verificador {
	
	public static void verificar(Ambiente objetoBD, Ambiente objeto){
		//Verifica o ambiente
		Assert.assertEquals(objetoBD.getDescricao(), 	objeto.getDescricao());
		Assert.assertEquals(objetoBD.getId(), 			objeto.getId());
		Assert.assertEquals(objetoBD.getNome(),			objeto.getNome());
		
		//Verifica o endereco
		verificar(objetoBD.getEndereco(), 		objeto.getEndereco());
		
		//Verifica os setores
		List<Setor> setoresBD = new ArrayList<Setor>();
		setoresBD.addAll(objetoBD.getSetores());
		for (Setor setor : objeto.getSetores()) {
			verificar(setoresBD.get(setoresBD.indexOf(setor)), setor);
		}
	}	
	
	public static void verificar(Artista objetoBD, Artista objeto){
		Assert.assertEquals(objetoBD.getId(), 			objeto.getId());
		Assert.assertEquals(objetoBD.getNome(),			objeto.getNome());
	}
	
	public static void verificar(Diretor objetoBD, Diretor objeto){
		Assert.assertEquals(objetoBD.getId(), 			objeto.getId());
		Assert.assertEquals(objetoBD.getNome(),			objeto.getNome());
	}
	
	public static void verificar(Endereco objetoBD, Endereco objeto){
		Assert.assertEquals(objetoBD.getBairro(), 		objeto.getBairro());
		Assert.assertEquals(objetoBD.getCidade(), 		objeto.getCidade());
		Assert.assertEquals(objetoBD.getComplemento(), 	objeto.getComplemento());
		Assert.assertEquals(objetoBD.getEstado(), 		objeto.getEstado());
		Assert.assertEquals(objetoBD.getId(), 			objeto.getId());
		Assert.assertEquals(objetoBD.getNumero(),		objeto.getNumero());
		Assert.assertEquals(objetoBD.getPais(), 		objeto.getPais());
		Assert.assertEquals(objetoBD.getRua(), 			objeto.getRua());
	}	
	
	public static void verificar(Evento objectBD, Evento object){
		//Verifica o evento
		Assert.assertEquals(objectBD.getCensura(), 		object.getCensura());
		Assert.assertEquals(DateUtils.minimizeDate(objectBD.getDataEvento()), 	DateUtils.minimizeDate(object.getDataEvento()));
		Assert.assertEquals(objectBD.getDescricao(), 	object.getDescricao());
		Assert.assertEquals(objectBD.getId(), 			object.getId());
		Assert.assertEquals(objectBD.getNome(), 		object.getNome());
		
		//Verifica Diretor
		verificar(objectBD.getDiretor(), object.getDiretor());
		
		//verifica os Artistas
		List<Artista> artistasBD = new ArrayList<Artista>();
		artistasBD.addAll(objectBD.getArtistas());
		for (Artista artista : object.getArtistas()) {
			verificar(artistasBD.get(artistasBD.indexOf(artista)), artista);
		}
		
		//Verifica o ambiente
		verificar(objectBD.getAmbiente(), object.getAmbiente());
		
		//verificar atributos especificos
		switch (object.getTipoEventoEnum()) {
		case FILME:
			Assert.assertEquals(((Filme)objectBD).getGenero(), ((Filme)object).getGenero());	
			break;
		case PECA:
			Assert.assertEquals(((Peca)objectBD).getGenero(), ((Peca)object).getGenero());	
			Assert.assertEquals(((Peca)objectBD).getCompanhia(), ((Peca)object).getCompanhia());	
			break;
		case SHOW:
			Assert.assertEquals(((Show)objectBD).getEstilo(), ((Show)object).getEstilo());	
		}
	}
	
	public static void verificar(Ingresso objetoBD, Ingresso objeto){
		Assert.assertEquals(objetoBD.getId(), 		objeto.getId());
	}
	
	public static void verificar(Pessoa objetoBD, Pessoa objeto){
		//Verifica o Usuario
		Assert.assertEquals(DateUtils.minimizeDate(objetoBD.getDataNascimento()), 	DateUtils.minimizeDate(objeto.getDataNascimento()));
		Assert.assertEquals(objetoBD.getEmail(), 			objeto.getEmail());
		Assert.assertEquals(objetoBD.getId(), 				objeto.getId());
		Assert.assertEquals(objetoBD.getNome(),				objeto.getNome());
		Assert.assertEquals(objetoBD.getSenha(), 			objeto.getSenha());
		Assert.assertEquals(objetoBD.getTelefone(), 		objeto.getTelefone());
		Assert.assertEquals(objetoBD.getUsuario(), 			objeto.getUsuario());
		
		//Verifica o endereco
		verificar(objetoBD.getEndereco(), 			objeto.getEndereco());
		
		//verificar atributos especificos
		switch (objeto.getTipoPessoaEnum()) {
		case CLIENTE:
			Assert.assertEquals(((Cliente)objetoBD).getDocumento(), ((Cliente)objeto).getDocumento());	
			Assert.assertEquals(((Cliente)objetoBD).getNumeroCartao(), ((Cliente)objeto).getNumeroCartao());	
			break;
		case FUNCIONARIO:
			Assert.assertEquals(((Funcionario)objetoBD).getCodigo(), ((Funcionario)objeto).getCodigo());	
			Assert.assertEquals(((Funcionario)objetoBD).getContaCorrente(), ((Funcionario)objeto).getContaCorrente());	
		}
	}
	
	public static void verificar(Setor objetoBD, Setor objeto){
		Assert.assertEquals(objetoBD.getCapacidade(), 	objeto.getCapacidade());
		Assert.assertEquals(objetoBD.getId(), 			objeto.getId());
		Assert.assertEquals(objetoBD.getNome(), 			objeto.getNome());
		Assert.assertEquals(objetoBD.getPreco(), 		objeto.getPreco(), 0);
	}

	public static void verificar(Venda objetoBD, Venda objeto){
		Assert.assertEquals(objetoBD.getId(), 			objeto.getId());
		Assert.assertEquals(objetoBD.getTotal(), 		objeto.getTotal());
		
		verificar(objetoBD.getCliente(), objeto.getCliente());
		
		List<Ingresso> ingressosBD = new ArrayList<Ingresso>();
		ingressosBD.addAll(objetoBD.getIngressos());
		for (Ingresso ingresso : objeto.getIngressos()) {
			verificar(ingressosBD.get(ingressosBD.indexOf(ingresso)), ingresso);
		}
	}	
	
}
