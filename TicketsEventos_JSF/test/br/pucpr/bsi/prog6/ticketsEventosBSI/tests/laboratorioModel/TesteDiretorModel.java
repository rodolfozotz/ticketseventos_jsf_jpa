package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioModel;

import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.DiretorEnum.BAE_YONG_KYUN;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.DiretorEnum.CLARENCE_BADGER;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.DiretorEnum.HECTOR_BABENCO;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.DiretorEnum.JOHN_BADHAM;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.DiretorEnum.KING_BAGGOT;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.DiretorEnum.LLOYD_BACON;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.DiretorEnum.ROY_WARD_BAKER;

import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Diretor;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.DiretorEnum;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

@RunWith(Parameterized.class)
public class TesteDiretorModel {

	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////		
	protected DiretorEnum diretorEnum;
	protected Diretor diretor;

	@SuppressWarnings("rawtypes")
	@Parameterized.Parameters
	public static Collection listaObjetos() {
		return Arrays.asList(new Object[][] {
				{ HECTOR_BABENCO },
				{ LLOYD_BACON },
				{ CLARENCE_BADGER },
				{ JOHN_BADHAM },
				{ BAE_YONG_KYUN },
				{ KING_BAGGOT },
				{ ROY_WARD_BAKER }
		});
	}	
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TesteDiretorModel(DiretorEnum diretorEnum) {
		this.diretorEnum = diretorEnum;
	}
	
	
	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////	
	
	/**
	 * Metodo para criar um novo Diretor
	 * @return
	 */
	public static Diretor novoDiretor(DiretorEnum diretorEnum){
		Diretor diretor = new Diretor();
		diretor.setNome(diretorEnum.getNome());
		return diretor;
	}
	
	@Before
	public void criaDiretor(){
		this.diretor = novoDiretor(this.diretorEnum);
	}
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////
	
	@Test
	public void criar() {
		//Verifica se nao eh nulo
		Assert.assertNotNull(diretor);
		
		//Verifica se os atributos estao preenchidos
		Assert.assertTrue(StringUtils.isNotBlank(diretor.getNome()));
		
		//Alteracao dos atributos
		DiretorEnum diretorEnumTemp = DiretorEnum.getDiretor();
		diretor.setNome(diretorEnumTemp.getNome());
		
		//Verifica se alteracao dos atributos esta sendo realizada
		Assert.assertEquals(diretor.getNome(), diretorEnumTemp.getNome());
		
		//Volta os atributos, para ficar compativel com outros testes		
		diretor.setNome(this.diretorEnum.getNome());
	}
}
