package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioDAO.modification;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.AmbienteBCFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.ArtistaBC;
import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.DiretorBC;
import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.EventoBCFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Ambiente;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.AmbienteFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Diretor;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Evento;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.EventoFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.enums.TipoAmbienteEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.enums.TipoEventoEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.ArtistaEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioBC.TesteEventoBC;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.util.Atualizador;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.util.Verificador;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

@SuppressWarnings("unchecked")
public class TesteEventoDAOModification extends TesteEventoBC {
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TesteEventoDAOModification(EventoEnum evento) {
		super(evento);
	}

	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////	
	
	/**
	 * Metodo para criar um novo Evento
	 * @return
	 */
	public static Evento novoEvento(EventoEnum eventoEnum){
		
		//Para obter os artistas do BD
		ArtistaEnum[] arrayArtistas = eventoEnum.getArtistasEnum();
		List<Artista> artistas = new ArrayList<Artista>();
		for (ArtistaEnum artistaEnum : arrayArtistas) {
			Artista filter = new Artista();
			filter.setNome(artistaEnum.getNome());
			artistas.add(ArtistaBC.getInstance().findByFilterPlusEventos(filter).get(0));
		}
		
		//Para obter o ambiente do BD
		Ambiente filter = AmbienteFactory.getInstance(TipoAmbienteEnum.getBy(eventoEnum.getAmbienteEnum().getTipoAmbiente()),new Endereco());
		filter.setNome(eventoEnum.getAmbienteEnum().getNome());
		List<Ambiente> ambientes = AmbienteBCFactory.getInstance(filter.getTipoAmbienteEnum()).findByFilterPlusEventos(filter);

		
		//Para obter o diretor
		Diretor filterDiretor = new Diretor();
		filterDiretor.setNome(eventoEnum.getDiretorEnum().getNome());
		List<Diretor> diretorsFilter = DiretorBC.getInstance().findByFilterPlusEventos(filterDiretor);
		
		Evento evento;
		if(artistas.size() == 1){
			evento = EventoFactory.getInstance(TipoEventoEnum.getBy(eventoEnum.getTipo()),
					ambientes.get(0), 
					artistas.get(0), 
					diretorsFilter.get(0),
					eventoEnum.getComissao());
		} else {
			evento = EventoFactory.getInstance(TipoEventoEnum.getBy(eventoEnum.getTipo()),
					ambientes.get(0), 
					artistas, 
					diretorsFilter.get(0),
					eventoEnum.getComissao());
		}
		
		evento.setCensura(eventoEnum.getCensura());
		evento.setDataEvento(eventoEnum.getDataEvento());
		evento.setDescricao(eventoEnum.getDescricao());
		evento.setNome(eventoEnum.getNome());
		
		mudarAtributosEspecificos(eventoEnum, evento);
		
		return evento;
	}
	
	@Before
	public void criaEvento(){
		this.evento = novoEvento(this.eventoEnum);
	}

	
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Metodo responsavel por realizar um INSERT na base de Dados
	 */
	@Test
	@Override
	public void criar(){
		//Realiza o INSERT na chamada do metodo para o BC
		//DICA: Caso não esteja rodando aqui, veja se voce esta chamando a classe DAO a partir da BC
		super.criar();
		
		//Verifica se o id eh maior que zero
		Assert.assertTrue(evento.getId() > 0);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Verificador.verificar((Evento)EventoBCFactory.getInstance(evento.getTipoEventoEnum()).findById(evento.getId()), evento);
	}
	
	/**
	 * Metodo responsavel por atualizar um evento completo
	 */
	@Test
	public void updateEventoCompleto(){
		//Realiza o INSERT na chamada do metodo para o BC
		criar();
		
		//Atualiza Atributos
		Atualizador.atualizar(evento, "F-");
		
		//Atualiza o objeto no BD
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).update(evento);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Verificador.verificar((Evento)EventoBCFactory.getInstance(evento.getTipoEventoEnum()).findById(evento.getId()), evento);
		
		//Remove o objeto do BD - Para nao dar problemas no teste de queries
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).delete(evento);
	}
	
	/**
	 * Metodo responsavel por deletar um evento completo
	 */
	@Test
	public void deletarEventoCompleto(){
		//Realiza o INSERT na chamada do metodo para o BC
		criar();

		//Remove o objeto do BD
		EventoBCFactory.getInstance(evento.getTipoEventoEnum()).delete(evento);
		
		//Verifica se o objeto deixou de existir no BD
		Assert.assertNull(EventoBCFactory.getInstance(evento.getTipoEventoEnum()).findById(evento.getId()));
	}
}
