package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioBC;

import org.junit.Test;

import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.DiretorBC;
import br.pucpr.bsi.prog6.ticketsEventosBSI.exception.BSIException;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.DiretorEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioModel.TesteDiretorModel;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

public class TesteDiretorBC extends TesteDiretorModel{
	
	///////////////////////////////////////////
	// CONSTRUTORES
	///////////////////////////////////////////
	
	public TesteDiretorBC(DiretorEnum diretor) {
		super(diretor);
	}
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Metodo responsavel por criar um diretor completo
	 */
	@Test
	@Override
	public void criar(){
		super.criar();
		DiretorBC.getInstance().insert(diretor);
	}
	
	/**
	 * Valida o diretor nulo
	 */
	@Test(expected = BSIException.class)
	public void validarDiretorNulo(){
		DiretorBC.getInstance().insert(null);
	}
	
	/**
	 * Valida o diretor com o nome preenchido com espacos em branco
	 */
	@Test(expected = BSIException.class)
	public void validarDiretorEspacosBranco(){
		diretor.setNome("                              ");
		DiretorBC.getInstance().insert(diretor);
	}
	
	/**
	 * Valida o diretor sem nome
	 */
	@Test(expected = BSIException.class)
	public void validarDiretorSemNome(){
		diretor.setNome(null);
		DiretorBC.getInstance().insert(diretor);
	}
	
}
