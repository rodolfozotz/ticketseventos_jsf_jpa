package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums;

import java.util.Date;
import java.util.Random;

import br.pucpr.bsi.prog6.ticketsEventosBSI.utils.DateUtils;

public enum EventoEnum { 
	
	FILME_01('F', "Filme 01", "Descricao do Filme 01", "09/08/2015", 3, "Infantil", 10.0, AmbienteEnum.CINEMA_01, 
			DiretorEnum.BAE_YONG_KYUN, ArtistaEnum.FRANCIS_BACON, ArtistaEnum.LEON_BAKST),
	FILME_02('F', "Filme 02", "Descricao do Filme 02", "19/08/2015", 6, "Aventura", 15.0, AmbienteEnum.CINEMA_02, 
			DiretorEnum.HECTOR_BABENCO, ArtistaEnum.JOHN_BALDESSARI, ArtistaEnum.HANS_BALDUNG, ArtistaEnum.MIROSLAW_BALKA),
	FILME_03('F', "Filme 03", "Descricao do Filme 03", "21/08/2015", 9, "Romance", 18.0, AmbienteEnum.CINEMA_03, 
			DiretorEnum.LLOYD_BACON, ArtistaEnum.GIACOMO_BALLA),
	FILME_04('F', "Filme 04", "Descricao do Filme 04", "30/08/2015", 12, "Suspense", 17.0, AmbienteEnum.CINEMA_01, 
			DiretorEnum.CLARENCE_BADGER, ArtistaEnum.BALTHUS, ArtistaEnum.FRANCIS_BACON),
	FILME_05('F', "Filme 05", "Descricao do Filme 05", "05/08/2015", 15, "Terror", 12.0, AmbienteEnum.CINEMA_02, 
			DiretorEnum.JOHN_BADHAM, ArtistaEnum.LEON_BAKST, ArtistaEnum.JOHN_BALDESSARI),
	FILME_06('F', "Filme 06", "Descricao do Filme 06", "15/08/2015", 18, "Acao", 25.0, AmbienteEnum.CINEMA_03, 
			DiretorEnum.KING_BAGGOT, ArtistaEnum.HANS_BALDUNG, ArtistaEnum.MIROSLAW_BALKA),
			
	PECA_01('P', "Peca 01", "Descricao da Peca 01", "09/08/2015", 3, "Infantil", 18.0, "Cia de Teatro ABC", 
			AmbienteEnum.TEATRO_01, DiretorEnum.ROY_WARD_BAKER, ArtistaEnum.GIACOMO_BALLA, ArtistaEnum.BALTHUS),
	PECA_02('P', "Peca 02", "Descricao da Peca 02", "19/08/2015", 6, "Aventura", 30.0, "Cia de Teatro 123", 
			AmbienteEnum.TEATRO_02, DiretorEnum.HECTOR_BABENCO, ArtistaEnum.FRANCIS_BACON, ArtistaEnum.LEON_BAKST, ArtistaEnum.JOHN_BALDESSARI),
	PECA_03('P', "Peca 03", "Descricao da Peca 03", "21/08/2015", 9, "Romance", 7.0, "Cia de Teatro VIVA", 
			AmbienteEnum.TEATRO_03, DiretorEnum.LLOYD_BACON, ArtistaEnum.HANS_BALDUNG),
	PECA_04('P', "Peca 04", "Descricao da Peca 04", "30/08/2015", 12, "Suspense", 25.0, "Cia de Teatro ABC", 
			AmbienteEnum.TEATRO_01, DiretorEnum.CLARENCE_BADGER, ArtistaEnum.MIROSLAW_BALKA, ArtistaEnum.GIACOMO_BALLA),
	PECA_05('P', "Peca 05", "Descricao da Peca 05", "05/08/2015", 15, "Terror", 8.0, "Cia de Teatro 123", 
			AmbienteEnum.TEATRO_02, DiretorEnum.JOHN_BADHAM, ArtistaEnum.BALTHUS, ArtistaEnum.FRANCIS_BACON),
	PECA_06('P', "Peca 06", "Descricao da Peca 06", "15/08/2015", 18, "Acao", 13.0, "Cia de Teatro VIVA", 
			AmbienteEnum.TEATRO_03, DiretorEnum.BAE_YONG_KYUN, ArtistaEnum.LEON_BAKST, ArtistaEnum.JOHN_BALDESSARI),
			
	SHOW_01('S', "Show 01", "Descricao do Show 01", "09/08/2015", 3, 25.0, "Infantil", AmbienteEnum.CASA_SHOW_01, 
			DiretorEnum.KING_BAGGOT, ArtistaEnum.HANS_BALDUNG, ArtistaEnum.MIROSLAW_BALKA),
	SHOW_02('S', "Show 02", "Descricao do Show 02", "19/08/2015", 6, 15.0, "Pop", AmbienteEnum.CASA_SHOW_02, 
			DiretorEnum.ROY_WARD_BAKER, ArtistaEnum.GIACOMO_BALLA, ArtistaEnum.BALTHUS, ArtistaEnum.FRANCIS_BACON),
	SHOW_03('S', "Show 03", "Descricao do Show 03", "21/08/2015", 9, 35.0, "Dance", AmbienteEnum.CASA_SHOW_03, 
			DiretorEnum.HECTOR_BABENCO, ArtistaEnum.LEON_BAKST),
	SHOW_04('S', "Show 04", "Descricao do Show 04", "30/08/2015", 12, 40.0, "Blues", AmbienteEnum.CASA_SHOW_01, 
			DiretorEnum.LLOYD_BACON, ArtistaEnum.JOHN_BALDESSARI, ArtistaEnum.HANS_BALDUNG),
	SHOW_05('S', "Show 05", "Descricao do Show 05", "05/08/2015", 15, 56.0, "Rock", AmbienteEnum.CASA_SHOW_02, 
			DiretorEnum.CLARENCE_BADGER, ArtistaEnum.MIROSLAW_BALKA, ArtistaEnum.GIACOMO_BALLA),
	SHOW_06('S', "Show 06", "Descricao do Show 06", "15/08/2015", 18, 19.0, "Reggae", AmbienteEnum.CASA_SHOW_03, 
			DiretorEnum.JOHN_BADHAM, ArtistaEnum.BALTHUS, ArtistaEnum.FRANCIS_BACON);			
	

	private char tipo;
	private String nome, descricao, genero, dataEvento, companhia, estilo;
	private Integer censura;
	private Double comissao;
	private DiretorEnum diretorEnum;
	private ArtistaEnum[] artistasEnum;
	private AmbienteEnum ambienteEnum;
	
	private EventoEnum(char tipo, String nome, String descricao, String dataEvento, Integer censura, String genero, 
						Double comissao, AmbienteEnum ambiente, DiretorEnum diretor, ArtistaEnum... artistas) {
		this.tipo = tipo;
		this.nome = nome;
		this.descricao = descricao;
		this.dataEvento = dataEvento;
		this.censura = censura;
		this.genero = genero;
		this.comissao = comissao;
		this.diretorEnum = diretor;
		this.artistasEnum = artistas;
		this.ambienteEnum = ambiente;
	}
	
	private EventoEnum(char tipo, String nome, String descricao, String dataEvento, Integer censura, String genero, 
			Double comissao, String companhia, AmbienteEnum ambiente, DiretorEnum diretor, ArtistaEnum... artistas) {
		this.tipo = tipo;
		this.nome = nome;
		this.descricao = descricao;
		this.dataEvento = dataEvento;
		this.censura = censura;
		this.genero = genero;
		this.comissao = comissao;
		this.companhia = companhia;
		this.diretorEnum = diretor;
		this.artistasEnum = artistas;
		this.ambienteEnum = ambiente;
	}
	
	private EventoEnum(char tipo, String nome, String descricao, String dataEvento, Integer censura, Double comissao,   
			String estilo, AmbienteEnum ambiente, DiretorEnum diretor, ArtistaEnum... artistas) {
		this.tipo = tipo;
		this.nome = nome;
		this.descricao = descricao;
		this.dataEvento = dataEvento;
		this.censura = censura;
		this.comissao = comissao;
		this.estilo = estilo;
		this.diretorEnum = diretor;
		this.artistasEnum = artistas;
		this.ambienteEnum = ambiente;
	}	
	
	
	public char getTipo() {
		return tipo;
	}
	
	public String getEstilo() {
		return estilo;
	}
	
	public String getNome() {
		return nome;
	}
	
	public Double getComissao() {
		return comissao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public Date getDataEvento() {
		return DateUtils.converterDate(dataEvento);
	}
	
	public Integer getCensura() {
		return censura;
	}
	
	public String getGenero() {
		return genero;
	}
	
	public String getCompanhia() {
		return companhia;
	}
	
	public AmbienteEnum getAmbienteEnum() {
		return ambienteEnum;
	}
	
	public DiretorEnum getDiretorEnum() {
		return diretorEnum;
	}
	
	public ArtistaEnum[] getArtistasEnum() {
		return artistasEnum;
	}
	
	public static EventoEnum getEvento(){
		Random generator = new Random();
		EventoEnum[] array = values();
		int length = array.length;
		return array[generator.nextInt(length)];
	}
	
}
