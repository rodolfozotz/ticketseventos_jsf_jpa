package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioDAO.queries;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.EventoBCFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.exception.BSIException;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.AmbienteFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Diretor;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Evento;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.EventoFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Filme;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Peca;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Show;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.enums.TipoAmbienteEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.enums.TipoEventoEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.AmbienteEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.ArtistaEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.DiretorEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EnderecoEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.SetorEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.util.Verificador;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * IMPORTANTE: Para rodar esses teste, limpar a base do Banco de Dados antes e rodar os testes da camada BC.
 * 
 * @author Mauda
 * 
 */

@SuppressWarnings("unchecked")
public class TesteEventoDAOQueries {
	
	///////////////////////////////////////////////////////////
	// TESTE DOS METODOS findAll E findById 
	///////////////////////////////////////////////////////////
	
	@Test
	public void testFindAlleFindById(){
		List<Evento> listaBD = EventoBCFactory.getInstance(TipoEventoEnum.FILME).findAll();
		listaBD.addAll(EventoBCFactory.getInstance(TipoEventoEnum.PECA).findAll());
		listaBD.addAll(EventoBCFactory.getInstance(TipoEventoEnum.FILME).findAll());
		
		//Verifica se a quantidade de instancias a mesma dos testes de populate
		Assert.assertEquals(listaBD.size(), EventoEnum.values().length);
		
		for (Evento eventoFindAll : listaBD) {
			Evento eventoFindId = (Evento) EventoBCFactory.getInstance(eventoFindAll.getTipoEventoEnum()).findById(eventoFindAll.getId());
			Verificador.verificar(eventoFindAll, eventoFindId);
		}
		
		//buscando um id inexistente
		Evento eventoIdInvalido = (Evento)EventoBCFactory.getInstance(TipoEventoEnum.FILME).findById(100000L);
		Assert.assertNull(eventoIdInvalido);
	}
	
	///////////////////////////////////////////////////////////
	// TESTES DO METODO findByFilter 
	///////////////////////////////////////////////////////////	
		
	@Test(expected = BSIException.class)
	public void testFindByFilterNulo(){
		EventoBCFactory.getInstance(TipoEventoEnum.FILME).findByFilter(null);
	}	
	
	@Test
	public void testFindByFilterNome(){
		for (EventoEnum eventoEnum : EventoEnum.values()) {
			Evento filter = EventoFactory.getInstance(TipoEventoEnum.getBy(eventoEnum.getTipo()), 
					AmbienteFactory.getInstance(TipoAmbienteEnum.getBy(eventoEnum.getAmbienteEnum().getTipoAmbiente()), new Endereco()),
					new ArrayList<Artista>(), new Diretor(), null);
			
			filter.setNome(eventoEnum.getNome());
			List<Evento> eventosFilter = EventoBCFactory.getInstance(TipoEventoEnum.getBy(eventoEnum.getTipo())).findByFilter(filter);
			
			//verifica se o tamanho do filtro eh 1
			Assert.assertEquals(eventosFilter.size(), 1);
			
			//Nomes sao iguais
			Assert.assertEquals(eventosFilter.get(0).getNome(), eventoEnum.getNome());
		}
	}
	
	@Test
	public void testFindByFilterDescricao(){
		for (EventoEnum eventoEnum : EventoEnum.values()) {
			Evento filter = EventoFactory.getInstance(TipoEventoEnum.getBy(eventoEnum.getTipo()), 
					AmbienteFactory.getInstance(TipoAmbienteEnum.getBy(eventoEnum.getAmbienteEnum().getTipoAmbiente()), new Endereco()),
					new ArrayList<Artista>(), new Diretor(), null);
			
			filter.setDescricao(eventoEnum.getDescricao());
			List<Evento> eventosFilter = EventoBCFactory.getInstance(TipoEventoEnum.getBy(eventoEnum.getTipo())).findByFilter(filter);
			
			//verifica se o tamanho do filtro eh 1
			Assert.assertEquals(eventosFilter.size(), 1);
			
			//Descricoes sao iguais
			Assert.assertEquals(eventosFilter.get(0).getDescricao(), eventoEnum.getDescricao());
		}
	}
	
	@Test
	public void testFindByFilterDataEvento(){
		for (EventoEnum eventoEnum : EventoEnum.values()) {
			Evento filter = EventoFactory.getInstance(TipoEventoEnum.getBy(eventoEnum.getTipo()), 
					AmbienteFactory.getInstance(TipoAmbienteEnum.getBy(eventoEnum.getAmbienteEnum().getTipoAmbiente()), new Endereco()),
					new ArrayList<Artista>(), new Diretor(), null);
			
			filter.setDataEvento(eventoEnum.getDataEvento());
			List<Evento> eventosFilter = EventoBCFactory.getInstance(TipoEventoEnum.getBy(eventoEnum.getTipo())).findByFilter(filter);
			
			//verifica se o tamanho do filtro eh 3
			Assert.assertEquals(eventosFilter.size(), 1);
			
			//Descricoes sao iguais
			Assert.assertEquals(eventosFilter.get(0).getDescricao(), eventoEnum.getDescricao());
		}
	}
	
	@Test
	public void testFindByFilterCensura(){
		for (EventoEnum eventoEnum : EventoEnum.values()) {
			Evento filter = EventoFactory.getInstance(TipoEventoEnum.getBy(eventoEnum.getTipo()), 
					AmbienteFactory.getInstance(TipoAmbienteEnum.getBy(eventoEnum.getAmbienteEnum().getTipoAmbiente()), new Endereco()),
					new ArrayList<Artista>(), new Diretor(), null);
			
			filter.setCensura(eventoEnum.getCensura());
			List<Evento> eventosFilter = EventoBCFactory.getInstance(TipoEventoEnum.getBy(eventoEnum.getTipo())).findByFilter(filter);
			
			//verifica se o tamanho do filtro eh 1
			Assert.assertEquals(eventosFilter.size(), 1);
			
			//Descricoes sao iguais
			Assert.assertEquals(eventosFilter.get(0).getDescricao(), eventoEnum.getDescricao());
		}
	}
	
	@Test
	public void testFindByFilterComissao(){
		for (EventoEnum eventoEnum : EventoEnum.values()) {
			Evento filter = EventoFactory.getInstance(TipoEventoEnum.getBy(eventoEnum.getTipo()), 
					AmbienteFactory.getInstance(TipoAmbienteEnum.getBy(eventoEnum.getAmbienteEnum().getTipoAmbiente()), new Endereco()),
					new ArrayList<Artista>(), new Diretor(), null);
			
			filter.setComissao(eventoEnum.getComissao());
			List<Evento> eventosFilter = EventoBCFactory.getInstance(TipoEventoEnum.getBy(eventoEnum.getTipo())).findByFilter(filter);
			
			//verifica se o tamanho do filtro eh 1
			Assert.assertEquals(eventosFilter.size(), 1);
			
			//Descricoes sao iguais
			Assert.assertEquals(eventosFilter.get(0).getDescricao(), eventoEnum.getDescricao());
		}
	}
	
	/////////////////////////////////////////////////////////
	// DADOS ESPECIFICOS DO EVENTO
	/////////////////////////////////////////////////////////
	
	/**
	 * Valida o evento sem companhia
	 */
	@Test
	public void validarEventoSemCompanhia(){
		for (EventoEnum eventoEnum : EventoEnum.values()) {
			if(TipoEventoEnum.getBy(eventoEnum.getTipo()).equals(TipoEventoEnum.PECA)){
				Evento filter = EventoFactory.getInstance(TipoEventoEnum.PECA, 
						AmbienteFactory.getInstance(TipoAmbienteEnum.getBy(eventoEnum.getAmbienteEnum().getTipoAmbiente()), new Endereco()),
						new ArrayList<Artista>(), new Diretor(), null);
				
				((Peca)filter).setCompanhia(eventoEnum.getCompanhia());
				List<Evento> eventosFilter = EventoBCFactory.getInstance(TipoEventoEnum.getBy(eventoEnum.getTipo())).findByFilter(filter);
				
				//verifica se o tamanho do filtro eh 2
				Assert.assertEquals(eventosFilter.size(), 2);
				
				//Descricoes sao iguais
				Assert.assertEquals(eventosFilter.get(0).getDescricao(), eventoEnum.getDescricao());
			}
		}
	}
	
	/**
	 * Valida o evento sem estilo
	 */
	@Test
	public void validarEventoSemEstilo(){
		for (EventoEnum eventoEnum : EventoEnum.values()) {
			if(TipoEventoEnum.getBy(eventoEnum.getTipo()).equals(TipoEventoEnum.SHOW)){
				Evento filter = EventoFactory.getInstance(TipoEventoEnum.SHOW, 
						AmbienteFactory.getInstance(TipoAmbienteEnum.getBy(eventoEnum.getAmbienteEnum().getTipoAmbiente()), new Endereco()),
						new ArrayList<Artista>(), new Diretor(), null);
				
				((Show)filter).setEstilo(eventoEnum.getEstilo());
				List<Evento> eventosFilter = EventoBCFactory.getInstance(TipoEventoEnum.getBy(eventoEnum.getTipo())).findByFilter(filter);
				
				//verifica se o tamanho do filtro eh 1
				Assert.assertEquals(eventosFilter.size(), 1);
				
				//Descricoes sao iguais
				Assert.assertEquals(eventosFilter.get(0).getDescricao(), eventoEnum.getDescricao());
			}
		}
	}
	
	/**
	 * Valida o evento sem genero
	 */
	@Test
	public void validarEventoSemGenero(){
		for (EventoEnum eventoEnum : EventoEnum.values()) {
			TipoEventoEnum tipo = TipoEventoEnum.getBy(eventoEnum.getTipo());
			if(!tipo.equals(TipoEventoEnum.SHOW)){
				Evento filter = EventoFactory.getInstance(tipo, 
						AmbienteFactory.getInstance(TipoAmbienteEnum.getBy(eventoEnum.getAmbienteEnum().getTipoAmbiente()), new Endereco()),
						new ArrayList<Artista>(), new Diretor(), null);
				
				if(tipo.equals(TipoEventoEnum.FILME)){
					((Filme)filter).setGenero(eventoEnum.getGenero());
				} else if(tipo.equals(TipoEventoEnum.PECA)){
					((Peca)filter).setGenero(eventoEnum.getGenero());
				}
				
				List<Evento> eventosFilter = EventoBCFactory.getInstance(TipoEventoEnum.getBy(eventoEnum.getTipo())).findByFilter(filter);
				
				//verifica se o tamanho do filtro eh 1
				Assert.assertEquals(eventosFilter.size(), 1);
				
				//Descricoes sao iguais
				Assert.assertEquals(eventosFilter.get(0).getDescricao(), eventoEnum.getDescricao());
			}
		}
	}

	/////////////////////////////////////////////////////////
	// DADOS DO ARTISTA
	/////////////////////////////////////////////////////////
	
	@Test
	public void testFindByFilterNomeArtista(){
		for(TipoEventoEnum tipoEventoEnum : TipoEventoEnum.values()){
			for (ArtistaEnum artistaEnum : ArtistaEnum.values()) {
				Evento filter = EventoFactory.getInstance(tipoEventoEnum, 
						AmbienteFactory.getInstance(TipoAmbienteEnum.CASASHOW, new Endereco()), 
						new ArrayList<Artista>(), new Diretor(), null);
				
				
				Artista a = new Artista();
				a.setNome(artistaEnum.getNome());
				filter.getArtistas().add(a);
				List<Evento> eventosFilter = EventoBCFactory.getInstance(tipoEventoEnum).findByFilter(filter);
				
				for (Evento evento : eventosFilter) {
					boolean existeArtista = false;
					for(Artista artista : evento.getArtistas()){
						if(artistaEnum.getNome().equals(artista.getNome())){
							existeArtista = true;
							break;
						}
					}
					Assert.assertTrue(existeArtista);
				}
			}
		}
	}	
	
	/////////////////////////////////////////////////////////
	// DADOS DO DIRETOR
	/////////////////////////////////////////////////////////
	@Test
	public void testFindByFilterNomeDiretor(){
		for(TipoEventoEnum tipoEventoEnum : TipoEventoEnum.values()){
			for (DiretorEnum diretorEnum : DiretorEnum.values()) {
				Evento filter = EventoFactory.getInstance(tipoEventoEnum, 
						AmbienteFactory.getInstance(TipoAmbienteEnum.CINEMA, new Endereco()), 
						new ArrayList<Artista>(), new Diretor(), null);
				
				filter.getDiretor().setNome(diretorEnum.getNome());
				List<Evento> eventosFilter = EventoBCFactory.getInstance(tipoEventoEnum).findByFilter(filter);
				
				for (Evento evento : eventosFilter) {
					Assert.assertEquals(diretorEnum.getNome(), evento.getDiretor().getNome());
				}
			}
		}
	}	
	
	/////////////////////////////////////////////////////////
	// DADOS DO AMBIENTE
	/////////////////////////////////////////////////////////
	@Test
	public void testFindByFilterNomeAmbiente(){
		for(TipoEventoEnum tipoEventoEnum : TipoEventoEnum.values()){
			for (AmbienteEnum ambienteEnum : AmbienteEnum.values()) {
				Evento filter = EventoFactory.getInstance(tipoEventoEnum, 
						AmbienteFactory.getInstance(TipoAmbienteEnum.getBy(ambienteEnum.getTipoAmbiente()), new Endereco()),
						new ArrayList<Artista>(), new Diretor(), null);
				
				filter.getAmbiente().setNome(ambienteEnum.getNome());
				List<Evento> eventosFilter = EventoBCFactory.getInstance(tipoEventoEnum).findByFilter(filter);
				
				for (Evento evento : eventosFilter) {
					Assert.assertEquals(ambienteEnum.getNome(), evento.getAmbiente().getNome());
				}
			}
		}
	}	
	
	/////////////////////////////////////////////////////////
	// DADOS DO ENDERECO DO AMBIENTE
	/////////////////////////////////////////////////////////
	@Test
	public void testFindByFilterCidadeAmbiente(){
		for(TipoEventoEnum tipoEventoEnum : TipoEventoEnum.values()){
			for (EnderecoEnum enderecoEnum : EnderecoEnum.values()) {
				Evento filter = EventoFactory.getInstance(tipoEventoEnum, 
						AmbienteFactory.getInstance(TipoAmbienteEnum.CASASHOW, new Endereco()),
						new ArrayList<Artista>(), new Diretor(), null);
				
				filter.getAmbiente().getEndereco().setCidade(enderecoEnum.getCidade());
				List<Evento> eventosFilter = EventoBCFactory.getInstance(tipoEventoEnum).findByFilter(filter);
				
				for (Evento evento : eventosFilter) {
					Assert.assertEquals(enderecoEnum.getCidade(), evento.getAmbiente().getEndereco().getCidade());
				}
			}
		}
	}
	
	/////////////////////////////////////////////////////////
	// DADOS DO SETOR DO AMBIENTE
	/////////////////////////////////////////////////////////
	@Test
	public void testFindByFilterCapacidadeSetorAmbiente(){
		for(TipoEventoEnum tipoEventoEnum : TipoEventoEnum.values()){
			for (SetorEnum setorEnum : SetorEnum.values()) {
				Evento filter = EventoFactory.getInstance(tipoEventoEnum, 
						AmbienteFactory.getInstance(TipoAmbienteEnum.CINEMA, new Endereco()),
						new ArrayList<Artista>(), new Diretor(), null);
				
				Setor setor = new Setor(filter.getAmbiente());
				setor.setCapacidade(setorEnum.getCapacidade());
				List<Evento> eventosFilter = EventoBCFactory.getInstance(tipoEventoEnum).findByFilter(filter);
				
				for (Evento evento : eventosFilter) {
					boolean existeSetor = false;
					for(Setor setorFilter : evento.getAmbiente().getSetores()){
						if(setorEnum.getCapacidade().equals(setorFilter.getCapacidade())){
							existeSetor = true;
							break;
						}
					}
					Assert.assertTrue(existeSetor);
				}
			}
		}
	}
	
}
