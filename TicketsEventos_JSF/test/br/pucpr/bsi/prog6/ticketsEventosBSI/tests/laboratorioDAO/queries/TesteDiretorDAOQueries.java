package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioDAO.queries;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.DiretorBC;
import br.pucpr.bsi.prog6.ticketsEventosBSI.exception.BSIException;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Diretor;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.DiretorEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.util.Verificador;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * IMPORTANTE: Para rodar esses teste, limpar a base do Banco de Dados antes e rodar os testes da camada BC.
 * 
 * @author Mauda
 * 
 */

public class TesteDiretorDAOQueries {
	
	///////////////////////////////////////////////////////////
	// TESTE DOS METODOS findAll E findById 
	///////////////////////////////////////////////////////////
	
	@Test
	public void testFindAlleFindById(){
		List<Diretor> listaBD = DiretorBC.getInstance().findAll();
		
		//Verifica se a quantidade de instancias a mesma dos testes de populate
		Assert.assertEquals(listaBD.size(), DiretorEnum.values().length);
		
		for (Diretor diretorFindAll : listaBD) {
			Diretor diretorFindId = DiretorBC.getInstance().findById(diretorFindAll.getId());
			Verificador.verificar(diretorFindAll, diretorFindId);
		}
		
		//buscando um id inexistente
		Diretor diretorIdInvalido = DiretorBC.getInstance().findById(100000L);
		Assert.assertNull(diretorIdInvalido);
	}
	
	///////////////////////////////////////////////////////////
	// TESTES DO METODO findByFilter 
	///////////////////////////////////////////////////////////	
		
	@Test(expected = BSIException.class)
	public void testFindByFilterNulo(){
		DiretorBC.getInstance().findByFilter(null);
	}	
	
	@Test(expected = BSIException.class)
	public void testFindByFilterVazio(){
		DiretorBC.getInstance().findByFilter(new Diretor());
	}	
	
	@Test
	public void testFindByFilterNome(){
		Diretor filter = new Diretor();
		for (DiretorEnum diretorEnum : DiretorEnum.values()) {
			filter.setNome(diretorEnum.getNome());
			List<Diretor> diretorsFilter = DiretorBC.getInstance().findByFilter(filter);
			
			//verifica se o tamanho do filtro eh 1
			Assert.assertEquals(diretorsFilter.size(), 1);
			
			//Nomes sao iguais
			Assert.assertEquals(diretorsFilter.get(0).getNome(), diretorEnum.getNome());
		}
	}
	
}
