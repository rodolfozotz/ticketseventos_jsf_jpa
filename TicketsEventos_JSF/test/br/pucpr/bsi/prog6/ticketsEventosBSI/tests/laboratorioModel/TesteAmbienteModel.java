package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioModel;

import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.AmbienteEnum.CASA_SHOW_01;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.AmbienteEnum.CASA_SHOW_02;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.AmbienteEnum.CASA_SHOW_03;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.AmbienteEnum.CINEMA_01;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.AmbienteEnum.CINEMA_02;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.AmbienteEnum.CINEMA_03;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.AmbienteEnum.TEATRO_01;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.AmbienteEnum.TEATRO_02;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.AmbienteEnum.TEATRO_03;

import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Ambiente;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.AmbienteFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.enums.TipoAmbienteEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.AmbienteEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.SetorEnum;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

@RunWith(Parameterized.class)
public class TesteAmbienteModel {

	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////		
	protected AmbienteEnum ambienteEnum;
	protected Ambiente ambiente;

	@SuppressWarnings("rawtypes")
	@Parameterized.Parameters
	public static Collection listaObjetos() {
		return Arrays.asList(new Object[][] {
				{ CASA_SHOW_01 },
				{ CASA_SHOW_02 },
				{ CASA_SHOW_03 },
				
				{ CINEMA_01 },
				{ CINEMA_02 },
				{ CINEMA_03 },
						
				{ TEATRO_01 },
				{ TEATRO_02 },
				{ TEATRO_03 }
		});
	}	
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TesteAmbienteModel(AmbienteEnum ambienteEnum) {
		this.ambienteEnum = ambienteEnum;
	}
	
	
	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////	
	
	private static void addSetor(SetorEnum setorEnum, Ambiente ambiente){
		Setor setor = new Setor(ambiente);
		setor.setCapacidade(setorEnum.getCapacidade());
		setor.setNome(setorEnum.getNome());
		setor.setPreco(setorEnum.getPreco());
	}
	
	
	/**
	 * Metodo para criar um novo Ambiente
	 * @return
	 */
	public static Ambiente novoAmbiente(AmbienteEnum ambienteEnum){
		//Inicializa ambiente
		Ambiente ambiente = AmbienteFactory.getInstance(
				TipoAmbienteEnum.getBy(ambienteEnum.getTipoAmbiente()),
				TesteEnderecoModel.novoEndereco(ambienteEnum.getEnderecoEnum()));
		
		ambiente.setNome(ambienteEnum.getNome());
		ambiente.setDescricao(ambienteEnum.getDescricao());
		for (SetorEnum setorEnum : ambienteEnum.getSetores()) {
			addSetor(setorEnum, ambiente);
		}
		return ambiente;
	}
	
	@Before
	public void criaAmbiente(){
		this.ambiente = novoAmbiente(this.ambienteEnum);
	}
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////
	
	@Test
	public void criar() {
		//Verifica se nao eh nulo
		Assert.assertNotNull(ambiente);
		
		//Verifica se os atributos estao preenchidos
		Assert.assertTrue(StringUtils.isNotBlank(ambiente.getDescricao()));
		Assert.assertTrue(StringUtils.isNotBlank(ambiente.getNome()));
		Assert.assertNotNull(ambiente.getCapacidade());
		Assert.assertNotNull(ambiente.getEndereco());
		Assert.assertNotNull(ambiente.getSetores());
		Assert.assertFalse(ambiente.getSetores().isEmpty());
		Assert.assertNotNull(ambiente.getTipoAmbienteEnum());
		
		int capacidade = 0;
		for(Setor setor: ambiente.getSetores()){
			//Verifica se o setor esta apontando para o ambiente
			Assert.assertEquals(setor.getAmbiente(), ambiente);
			
			//Aumenta o valor da variavel capacidade contabilizando o total do ambiente
			capacidade += setor.getCapacidade();

			Assert.assertTrue(StringUtils.isNotBlank(setor.getNome()));
			SetorEnum setorEnumTemp = SetorEnum.getSetor(setor.getNome());
			
			Assert.assertNotNull(setor.getCapacidade());
			Assert.assertEquals(setorEnumTemp.getCapacidade(), setor.getCapacidade(),0);
			
			Assert.assertNotNull(setor.getPreco());
			Assert.assertEquals(setorEnumTemp.getPreco(), setor.getPreco(), 0);
		}
		Assert.assertEquals(ambiente.getCapacidade(), capacidade, 0);
		
		//Alteracao dos atributos
		AmbienteEnum ambienteEnumTemp = AmbienteEnum.getAmbiente();
		ambiente.setNome(ambienteEnumTemp.getNome());
		ambiente.setDescricao(ambienteEnumTemp.getDescricao());
		
		//Verifica se alteracao dos atributos esta sendo realizada
		Assert.assertEquals(ambiente.getDescricao(), ambienteEnumTemp.getDescricao());
		Assert.assertEquals(ambiente.getNome(), ambienteEnumTemp.getNome());
		
		//Volta os atributos, para ficar compativel com outros testes		
		ambiente.setNome(this.ambienteEnum.getNome());
		ambiente.setDescricao(this.ambienteEnum.getDescricao());
	}
}
