package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums;

import java.util.Random;

public enum SetorEnum {
	
	PISTA_A("Pista A", 15, 15.00),
	PISTA_B("Pista B", 20, 12.00),
	PISTA_C("Pista C", 22, 10.00),
			
	CAMAROTE_A("Camarote A", 5, 150.00),
	CAMAROTE_B("Camarote B", 3, 300.00),
	CAMAROTE_C("Camarote C", 2, 500.00);
	
	private String nome;
	private Integer capacidade;
	private Double preco;
	
	private SetorEnum(String nome, Integer capacidade, Double preco) {
		this.nome = nome;
		this.capacidade = capacidade;
		this.preco = preco;
	}
	
	public String getNome() {
		return nome;
	}
	
	public Integer getCapacidade() {
		return capacidade;
	}
	
	public Double getPreco() {
		return preco;
	}
	
	public static SetorEnum getSetor(){
		Random generator = new Random();
		SetorEnum[] array = values();
		int length = array.length;
		return array[generator.nextInt(length)];
	}
	
	public static SetorEnum getSetor(String nome){
		if(nome != null){
			for(SetorEnum setor : values()){
				if(setor.getNome().equals(nome)){
					return setor;
				}
			}
		}
		return null;
	}
	
}
