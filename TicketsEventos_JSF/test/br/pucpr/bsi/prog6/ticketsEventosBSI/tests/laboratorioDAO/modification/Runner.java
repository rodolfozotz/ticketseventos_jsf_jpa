package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioDAO.modification;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioDAO.queries.TesteAmbienteDAOQueries;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioDAO.queries.TesteArtistaDAOQueries;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioDAO.queries.TesteDiretorDAOQueries;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioDAO.queries.TesteEventoDAOQueries;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioDAO.queries.TestePessoaDAOQueries;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioDAO.queries.TesteVendaDAOQueries;

/**
 * Essa classe realizara os testes da camada DAO de uma so vez.
 * Para que tudo ocorra com sucesso, a base devera estar vazia, pois existem metodos no
 * pacote Queries que necessitam que o numero de instancias da classe X esteja igual ao
 * numero descrito no Enum de testes da classe X.
 * Para limpar a base basta deletar os arquivos que se encontram na pasta banco e executar
 * novamente o script do banco de dados.
 * 
 * @author Mauda
 *
 */

public class Runner {
	
	private JUnitCore junit = new JUnitCore();
	
	private Results daoModification = new Results();
	private Results daoQueries = new Results();
	
	class Results{
		private Integer qtdTotal = 0;
		private Integer qtdErros = 0;
		private Integer qtdFailures = 0;
		
		public void report(String title){
			System.out.println("======================================================");
			System.out.println(title);
			System.out.println("Testes Totais: " + qtdTotal);
			System.out.println("Testes Success (Verdes): " + qtdSuccess());
			System.out.println("Testes Failures (Azuis): " + qtdFailures);
			System.out.println("Testes Errors (Vermelhos): " + qtdErros);
			System.out.println("======================================================\n\n");
		}
		
		public Integer qtdSuccess(){
			return qtdTotal - qtdErros - qtdFailures;
		}
	}
	
	@Test
	public void runner(){
		
		//Runner para a classe TesteArtistaDAOModification
		run(TesteArtistaDAOModification.class, daoModification);
		
		//Runner para a classe TesteDiretorDAOModification
		run(TesteDiretorDAOModification.class, daoModification);

		//Runner para a classe TestePessoaDAOModification
		run(TestePessoaDAOModification.class, daoModification);

		//Runner para a classe TesteAmbienteDAOModification
		run(TesteAmbienteDAOModification.class, daoModification);
		
		//Runner para a classe TesteEventoDAOModification
		run(TesteEventoDAOModification.class, daoModification);
		
		//Runner para a classe TesteVendaDAOModification
		run(TesteVendaDAOModification.class, daoModification);
		
		daoModification.report("RELATORIO DOS TESTES DAO MODIFICATION");
		
		
		
		
		//Runner para a classe TesteArtistaDAOQueries
		run(TesteArtistaDAOQueries.class, daoQueries);
		
		//Runner para a classe TesteDiretorDAOQueries
		run(TesteDiretorDAOQueries.class, daoQueries);
		
		//Runner para a classe TestePessoaDAOQueries
		run(TestePessoaDAOQueries.class, daoQueries);
		
		//Runner para a classe TesteAmbienteDAOQueries
		run(TesteAmbienteDAOQueries.class, daoQueries);
		
		//Runner para a classe TesteEventoDAOQueries
		run(TesteEventoDAOQueries.class, daoQueries);
		
		//Runner para a classe TesteVendaDAOQueries
		run(TesteVendaDAOQueries.class, daoQueries);

		
		daoQueries.report("RELATORIO DOS TESTES DAO QUERIES");
		
		
		
		
		System.out.println("======================================================");
		System.out.println("RELATORIO FINAL DOS TESTES DAO ");
		System.out.println("Testes Totais: " + (daoModification.qtdTotal + daoQueries.qtdTotal));
		System.out.println("Testes Success (Verdes): " + (daoModification.qtdSuccess() + daoQueries.qtdSuccess()));
		System.out.println("Testes Failures (Azuis): " + (daoModification.qtdFailures + daoQueries.qtdFailures));
		System.out.println("Testes Errors (Vermelhos): " + (daoModification.qtdErros + daoQueries.qtdErros));
		System.out.println("======================================================\n\n");
		
	}
	
	private void run(Class<?> classesTeste, Results results){
		Result r = junit.run(classesTeste);
		results.qtdTotal += r.getRunCount();
		for(Failure fail : r.getFailures()){
			if(fail.getException() instanceof AssertionError){
				results.qtdFailures++;
			} else {
				results.qtdErros++;
			}
		}
	}
	
}
