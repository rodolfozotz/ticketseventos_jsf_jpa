package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioBC;

import org.junit.Test;

import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.AmbienteBCFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.exception.BSIException;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.AmbienteEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioModel.TesteAmbienteModel;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */

@SuppressWarnings("unchecked")
public class TesteAmbienteBC extends TesteAmbienteModel {
	
	///////////////////////////////////////////
	// CONSTRUTORES
	///////////////////////////////////////////
	
	public TesteAmbienteBC(AmbienteEnum ambienteEnum) {
		super(ambienteEnum);
	}
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Metodo responsavel por criar um ambiente completo
	 */
	@Test
	@Override
	public void criar(){
		super.criar();
		AmbienteBCFactory.getInstance(ambiente.getTipoAmbienteEnum()).insert(ambiente);
	}
	
	/**
	 * Valida um ambiente nulo
	 */
	@Test(expected = BSIException.class)
	public void validarAmbienteNulo(){
		AmbienteBCFactory.getInstance(ambiente.getTipoAmbienteEnum()).insert(null);
	}
	
	/**
	 * Valida um ambiente com endereco nulo
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoNulo(){
		ambiente.setEndereco(null);
		AmbienteBCFactory.getInstance(ambiente.getTipoAmbienteEnum()).insert(ambiente);
	}	
	
	/**
	 * Valida um ambiente preenchido com espacos em branco
	 */
	@Test(expected = BSIException.class)
	public void validarAmbienteEspacosBranco(){
		ambiente.setNome("                              ");
		ambiente.setDescricao("                             ");
		AmbienteBCFactory.getInstance(ambiente.getTipoAmbienteEnum()).insert(ambiente);
	}	
	
	/**
	 * Valida um ambiente sem nome
	 */
	@Test(expected = BSIException.class)
	public void validarAmbienteSemNome(){
		ambiente.setNome(null);
		AmbienteBCFactory.getInstance(ambiente.getTipoAmbienteEnum()).insert(ambiente);
	}	
	
	/**
	 * Valida um ambiente sem descricao
	 */
	@Test(expected = BSIException.class)
	public void validarAmbienteSemDescricao(){
		ambiente.setDescricao(null);
		AmbienteBCFactory.getInstance(ambiente.getTipoAmbienteEnum()).insert(ambiente);
	}
	
	/**
	 * Valida um ambiente sem setores
	 */
	@Test(expected = BSIException.class)
	public void validarCasaShowSetorNula(){
		ambiente.setSetores(null);
		AmbienteBCFactory.getInstance(ambiente.getTipoAmbienteEnum()).insert(ambiente);
	}	
	
	/**
	 * Valida um setor sem capacidade
	 */
	@Test(expected = BSIException.class)
	public void validarAmbienteSemCapacidade(){
		ambiente.getSetores().get(0).setCapacidade(null);
		AmbienteBCFactory.getInstance(ambiente.getTipoAmbienteEnum()).insert(ambiente);
	}

	/**
	 * Valida um setor com capacidade negativa
	 */
	@Test(expected = BSIException.class)
	public void validarAmbienteComCapacidadeNegativa(){
		ambiente.getSetores().get(0).setCapacidade(-200);
		AmbienteBCFactory.getInstance(ambiente.getTipoAmbienteEnum()).insert(ambiente);
	}	
	
	/**
	 * Valida um setor sem nome
	 */
	@Test(expected = BSIException.class)
	public void validarSetorSemNome(){
		ambiente.getSetores().get(0).setNome(null);
		AmbienteBCFactory.getInstance(ambiente.getTipoAmbienteEnum()).insert(ambiente);
	}
	
	/**
	 * Valida um setor sem preco
	 */
	@Test(expected = BSIException.class)
	public void validarSetorSemPreco(){
		ambiente.getSetores().get(0).setPreco(null);
		AmbienteBCFactory.getInstance(ambiente.getTipoAmbienteEnum()).insert(ambiente);
	}
	
	/**
	 * Valida um setor com preco negativo
	 */
	@Test(expected = BSIException.class)
	public void validarSetorComPrecoNegativo(){
		ambiente.getSetores().get(0).setPreco(-200d);
		AmbienteBCFactory.getInstance(ambiente.getTipoAmbienteEnum()).insert(ambiente);
	}
}