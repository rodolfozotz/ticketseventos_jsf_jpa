package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioBC;

import org.junit.Test;

import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.ArtistaBC;
import br.pucpr.bsi.prog6.ticketsEventosBSI.exception.BSIException;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.ArtistaEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioModel.TesteArtistaModel;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

public class TesteArtistaBC extends TesteArtistaModel{
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TesteArtistaBC(ArtistaEnum artista) {
		super(artista);
	}
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Metodo responsavel por criar um artista completo
	 */
	@Test
	@Override
	public void criar(){
		super.criar();
		ArtistaBC.getInstance().insert(artista);
	}
	
	/**
	 * Valida o artista nulo
	 */
	@Test(expected = BSIException.class)
	public void validarArtistaNulo(){
		ArtistaBC.getInstance().insert(null);
	}
	
	/**
	 * Valida o artista com o nome preenchido com espacos em branco
	 */
	@Test(expected = BSIException.class)
	public void validarArtistaEspacosBranco(){
		artista.setNome("                              ");
		ArtistaBC.getInstance().insert(artista);
	}
	
	/**
	 * Valida o artista sem nome
	 */
	@Test(expected = BSIException.class)
	public void validarArtistaSemNome(){
		artista.setNome(null);
		ArtistaBC.getInstance().insert(artista);
	}
	
}
