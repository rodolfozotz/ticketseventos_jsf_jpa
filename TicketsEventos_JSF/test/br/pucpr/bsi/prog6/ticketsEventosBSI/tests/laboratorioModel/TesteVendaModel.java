package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioModel;

import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum.VENDA_FILME_01;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum.VENDA_FILME_02;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum.VENDA_FILME_03;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum.VENDA_FILME_04;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum.VENDA_FILME_05;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum.VENDA_FILME_06;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum.VENDA_PECA_01;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum.VENDA_PECA_02;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum.VENDA_PECA_03;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum.VENDA_PECA_04;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum.VENDA_PECA_05;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum.VENDA_PECA_06;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum.VENDA_SHOW_01;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum.VENDA_SHOW_02;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum.VENDA_SHOW_03;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum.VENDA_SHOW_04;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum.VENDA_SHOW_05;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum.VENDA_SHOW_06;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Cliente;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Evento;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Ingresso;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Venda;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

@RunWith(Parameterized.class)
public class TesteVendaModel {

	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////		
	protected VendaEnum vendaEnum;
	protected Venda venda;

	@SuppressWarnings("rawtypes")
	@Parameterized.Parameters
	public static Collection listaObjetos() {
		return Arrays.asList(new Object[][] {
				{ VENDA_FILME_01 },
				{ VENDA_FILME_02 },
				{ VENDA_FILME_03 },
				{ VENDA_FILME_04 },
				{ VENDA_FILME_05 },
				{ VENDA_FILME_06 },
				
				{ VENDA_PECA_01 },
				{ VENDA_PECA_02 },
				{ VENDA_PECA_03 },
				{ VENDA_PECA_04 },
				{ VENDA_PECA_05 },
				{ VENDA_PECA_06 },
				
				{ VENDA_SHOW_01 },
				{ VENDA_SHOW_02 },
				{ VENDA_SHOW_03 },
				{ VENDA_SHOW_04 },
				{ VENDA_SHOW_05 },
				{ VENDA_SHOW_06 }
		});
	}	
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TesteVendaModel(VendaEnum vendaEnum) {
		this.vendaEnum = vendaEnum;
	}
	
	
	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////	
	
	/**
	 * Metodo para criar um novo Venda
	 * @return
	 */
	public static Venda novaVenda(VendaEnum vendaEnum){
		
		//Para obter os artistas
		EventoEnum[] arrayFilmes = vendaEnum.getEventos();
		List<Evento> eventos = new ArrayList<Evento>();
		for (EventoEnum eventoEnum : arrayFilmes) {
			eventos.add(TesteEventoModel.novoEvento(eventoEnum));
		}
		List<Ingresso> ingressos = new ArrayList<Ingresso>();
		for (Evento evento : eventos) {
			ingressos.add(evento.getIngressoDisponivel(evento.getAmbiente().getSetores().get(0)));
		}
		
		Venda venda;
		if(ingressos.size() == 1){
			venda = new Venda((Cliente)TestePessoaModel.novaPessoa(vendaEnum.getPessoa()), 
					ingressos.get(0));
		} else {
			venda = new Venda((Cliente)TestePessoaModel.novaPessoa(vendaEnum.getPessoa()), 
					ingressos);
		}
		
		return venda;
	}
	
	@Before
	public void criaVenda(){
		this.venda = novaVenda(this.vendaEnum);
	}
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////
	
	@Test
	public void criar() {
		//Verifica se nao eh nulo
		Assert.assertNotNull(venda);
		
		//Verifica se os atributos estao preenchidos
		Assert.assertNotNull(venda.getCliente());
		Assert.assertNotNull(venda.getIngressos());
		
		//Pessoa contem a venda
		Assert.assertTrue(venda.getCliente().getVendas().contains(venda));
		
		//Ingresso contem a venda
		double valorTotalVenda = 0;
		for(Ingresso ingresso : venda.getIngressos()){
			valorTotalVenda += ingresso.getPreco();
			Assert.assertEquals(ingresso.getVenda(), venda);	
		}
		
		//Verifica se o valor total da venda eh o valor da soma dos ingressos
		Assert.assertEquals(venda.getTotal(), valorTotalVenda, 0);
	}
}
