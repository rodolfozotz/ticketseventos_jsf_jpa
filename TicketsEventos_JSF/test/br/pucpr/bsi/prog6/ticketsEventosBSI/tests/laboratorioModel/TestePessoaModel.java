package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioModel;

import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.PessoaEnum.CLIENTE_01;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.PessoaEnum.CLIENTE_02;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.PessoaEnum.CLIENTE_03;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.PessoaEnum.CLIENTE_04;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.PessoaEnum.CLIENTE_05;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.PessoaEnum.CLIENTE_06;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.PessoaEnum.FUNCIONARIO_01;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.PessoaEnum.FUNCIONARIO_02;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.PessoaEnum.FUNCIONARIO_03;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.PessoaEnum.FUNCIONARIO_04;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.PessoaEnum.FUNCIONARIO_05;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.PessoaEnum.FUNCIONARIO_06;

import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Cliente;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Funcionario;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Pessoa;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.PessoaFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.enums.TipoPessoaEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.PessoaEnum;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

@RunWith(Parameterized.class)
public class TestePessoaModel {

	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////		
	protected PessoaEnum pessoaEnum;
	protected Pessoa pessoa;

	@SuppressWarnings("rawtypes")
	@Parameterized.Parameters
	public static Collection listaObjetos() {
		return Arrays.asList(new Object[][] {
				{ CLIENTE_01 },
				{ CLIENTE_02 },
				{ CLIENTE_03 },
				{ CLIENTE_04 },
				{ CLIENTE_05 },
				{ CLIENTE_06 },
				
				{ FUNCIONARIO_01 },
				{ FUNCIONARIO_02 },
				{ FUNCIONARIO_03 },
				{ FUNCIONARIO_04 },
				{ FUNCIONARIO_05 },
				{ FUNCIONARIO_06 }
		});
	}	
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TestePessoaModel(PessoaEnum pessoaEnum) {
		this.pessoaEnum = pessoaEnum;
	}
	
	
	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////	
	
	/**
	 * Metodo para criar um novo Pessoa
	 * @return
	 */
	public static Pessoa novaPessoa(PessoaEnum pessoaEnum){
		Pessoa pessoa = PessoaFactory.getInstance(
				TipoPessoaEnum.getBy(pessoaEnum.getTipo()),
				TesteEnderecoModel.novoEndereco(pessoaEnum.getEnderecoEnum()));
		pessoa.setNome(pessoaEnum.getNome());
		pessoa.setEmail(pessoaEnum.getEmail());
		pessoa.setSenha(pessoaEnum.getSenha());
		pessoa.setTelefone(pessoaEnum.getTelefone());
		pessoa.setUsuario(pessoaEnum.getUsuario());
		pessoa.setDataNascimento(pessoaEnum.getDataNascimento());
		
		mudarAtributosEspecificos(pessoaEnum, pessoa);

		return pessoa;
	}
	
	public static void mudarAtributosEspecificos(PessoaEnum pessoaEnum, Pessoa pessoa){
		switch (pessoa.getTipoPessoaEnum()) {
			case CLIENTE:
				((Cliente)pessoa).setDocumento(pessoaEnum.getDocumento());
				((Cliente)pessoa).setNumeroCartao(pessoaEnum.getNumeroCartao());
				break;
			case FUNCIONARIO:
				((Funcionario)pessoa).setCodigo(pessoaEnum.getCodigo());
				((Funcionario)pessoa).setContaCorrente(pessoaEnum.getContaCorrente());
		}		
	}
	
	
	@Before
	public void criaPessoa(){
		this.pessoa = novaPessoa(this.pessoaEnum);
	}
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////
	
	@Test
	public void criar() {
		//Verifica se nao eh nulo
		Assert.assertNotNull(pessoa);
		
		//Verifica se os atributos estao preenchidos
		Assert.assertTrue(StringUtils.isNotBlank(pessoa.getEmail()));
		Assert.assertTrue(StringUtils.isNotBlank(pessoa.getNome()));
		Assert.assertTrue(StringUtils.isNotBlank(pessoa.getSenha()));
		Assert.assertTrue(StringUtils.isNotBlank(pessoa.getTelefone()));
		Assert.assertTrue(StringUtils.isNotBlank(pessoa.getUsuario()));
		Assert.assertNotNull(pessoa.getDataNascimento());
		Assert.assertNotNull(pessoa.getEndereco());
		
		switch (pessoa.getTipoPessoaEnum()) {
			case CLIENTE:
				Assert.assertTrue(StringUtils.isNotBlank(((Cliente)pessoa).getDocumento()));
				Assert.assertTrue(StringUtils.isNotBlank(((Cliente)pessoa).getNumeroCartao()));
				break;
			case FUNCIONARIO:
				Assert.assertTrue(StringUtils.isNotBlank(((Funcionario)pessoa).getCodigo()));
				Assert.assertTrue(StringUtils.isNotBlank(((Funcionario)pessoa).getContaCorrente()));
		}		
		
		//Alteracao dos atributos
		PessoaEnum pessoaEnumTemp = PessoaEnum.getPessoa();
		pessoa.setNome(pessoaEnumTemp.getNome());
		pessoa.setEmail(pessoaEnumTemp.getEmail());
		pessoa.setSenha(pessoaEnumTemp.getSenha());
		pessoa.setTelefone(pessoaEnumTemp.getTelefone());
		pessoa.setUsuario(pessoaEnumTemp.getUsuario());
		pessoa.setDataNascimento(pessoaEnumTemp.getDataNascimento());
		mudarAtributosEspecificos(pessoaEnumTemp, pessoa);
		
		//Verifica se alteracao dos atributos esta sendo realizada
		Assert.assertEquals(pessoa.getEmail(), pessoaEnumTemp.getEmail());
		Assert.assertEquals(pessoa.getNome(), pessoaEnumTemp.getNome());
		Assert.assertEquals(pessoa.getSenha(), pessoaEnumTemp.getSenha());
		Assert.assertEquals(pessoa.getTelefone(), pessoaEnumTemp.getTelefone());
		Assert.assertEquals(pessoa.getUsuario(), pessoaEnumTemp.getUsuario());
		Assert.assertEquals(pessoa.getDataNascimento(), pessoaEnumTemp.getDataNascimento());
		switch (pessoa.getTipoPessoaEnum()) {
		case CLIENTE:
			Assert.assertEquals(((Cliente)pessoa).getDocumento(), pessoaEnumTemp.getDocumento());
			Assert.assertEquals(((Cliente)pessoa).getNumeroCartao(), pessoaEnumTemp.getNumeroCartao());
			break;
		case FUNCIONARIO:
			Assert.assertEquals(((Funcionario)pessoa).getCodigo(), pessoaEnumTemp.getCodigo());
			Assert.assertEquals(((Funcionario)pessoa).getContaCorrente(), pessoaEnumTemp.getContaCorrente());
		}		
		
		//Volta os atributos, para ficar compativel com outros testes		
		pessoa.setNome(this.pessoaEnum.getNome());
		pessoa.setEmail(this.pessoaEnum.getEmail());
		pessoa.setSenha(this.pessoaEnum.getSenha());
		pessoa.setTelefone(this.pessoaEnum.getTelefone());
		pessoa.setUsuario(this.pessoaEnum.getUsuario());
		pessoa.setDataNascimento(this.pessoaEnum.getDataNascimento());
		mudarAtributosEspecificos(pessoaEnum, pessoa);
		
	}
}
