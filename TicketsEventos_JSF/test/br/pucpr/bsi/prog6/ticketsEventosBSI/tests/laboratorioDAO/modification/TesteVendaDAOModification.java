package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioDAO.modification;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.ClienteBC;
import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.EventoBCFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.IngressoBC;
import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.VendaBC;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Cliente;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Evento;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Ingresso;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Venda;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioBC.TesteVendaBC;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.util.Verificador;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

@SuppressWarnings("unchecked")
public class TesteVendaDAOModification extends TesteVendaBC {
	
	public boolean vendaCompleta = false;
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TesteVendaDAOModification(VendaEnum venda) {
		super(venda);
	}

	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////	
	
	/**
	 * Metodo para criar um novo Venda
	 * @return
	 */
	public static Venda novaVenda(VendaEnum vendaEnum){
		//Para obter os eventos do BD
		EventoEnum[] arrayFilmes = vendaEnum.getEventos();
		List<Evento> eventos = new ArrayList<Evento>();
		for (EventoEnum eventoEnum : arrayFilmes) {
			Evento filter = TesteEventoDAOModification.novoEvento(eventoEnum);
			eventos.add((Evento)EventoBCFactory.getInstance(filter.getTipoEventoEnum()).findByFilter(filter).get(0));
		}
			
		List<Ingresso> ingressos = new ArrayList<Ingresso>();
		for (Evento evento : eventos) {
			ingressos.add((Ingresso)IngressoBC.getInstance().findIngressosDisponiveis(evento, evento.getAmbiente().getSetores().get(0)).get(0));
		}
		
		//Para obter o cliente do BD
		Cliente filterPessoa = new Cliente(new Endereco());
		filterPessoa.setNome(vendaEnum.getPessoa().getNome());
		List<Cliente> pessoaFilter = ClienteBC.getInstance().findByFilterPlusVendas(filterPessoa);

		
		Venda venda;
		if(ingressos.size() == 1){
			venda = new Venda(pessoaFilter.get(0), 
					ingressos.get(0));
		} else {
			venda = new Venda(pessoaFilter.get(0), 
					ingressos);
		}
		
		return venda;
	}
	
	@Before
	public void criaVenda(){
		this.venda = novaVenda(this.vendaEnum);
		vendaCompleta = this.vendaEnum.isVendaCompleta();
	}

	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Metodo responsavel por realizar um INSERT na base de Dados
	 */
	@Test
	@Override
	public void criar(){
		//Realiza o INSERT na chamada do metodo para o BC
		//DICA: Caso nao esteja rodando aqui, veja se voce esta chamando a classe DAO a partir da BC
		super.criar();
		
		//Verifica se o id eh maior que zero
		Assert.assertTrue(venda.getId() > 0);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Verificador.verificar(VendaBC.getInstance().findById(venda.getId()), venda);
		
		if(vendaCompleta){
			VendaBC.getInstance().comprar(venda);
		}
	}
	
	/**
	 * Metodo responsavel por deletar um venda completo
	 */
	@Test
	public void deletarVendaCompleto(){
		//Realiza o INSERT na chamada do metodo para o BC
		criar();

		//Remove o objeto do BD
		VendaBC.getInstance().delete(venda);
		
		//Verifica se o objeto deixou de existir no BD
		Assert.assertNull(VendaBC.getInstance().findById(venda.getId()));
	}
}
