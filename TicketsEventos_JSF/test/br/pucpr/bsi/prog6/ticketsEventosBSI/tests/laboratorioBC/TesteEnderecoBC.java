package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioBC;

import org.junit.Test;

import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.EnderecoBC;
import br.pucpr.bsi.prog6.ticketsEventosBSI.exception.BSIException;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EnderecoEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioModel.TesteEnderecoModel;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

public class TesteEnderecoBC extends TesteEnderecoModel{
	
	///////////////////////////////////////////
	// CONSTRUTORES
	///////////////////////////////////////////
	
	public TesteEnderecoBC(EnderecoEnum endereco) {
		super(endereco);
	}
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Metodo responsavel por criar um endereco completo
	 */
	@Test
	@Override
	public void criar(){
		super.criar();
		EnderecoBC.getInstance().insert(endereco);
	}
	
	/**
	 * Valida o endereco nulo
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoNulo(){
		EnderecoBC.getInstance().insert(null);
	}
	
	/**
	 * Valida o endereco com o nome preenchido com espacos em branco
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoEspacosBranco(){
		endereco.setBairro("                         ");
		endereco.setCidade("                         ");
		endereco.setComplemento("                    ");
		endereco.setEstado("                         ");
		endereco.setPais("                           ");
		endereco.setRua("                            ");

		EnderecoBC.getInstance().insert(endereco);
	}
	
	/**
	 * Valida o endereco sem o nome da rua
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoSemRua(){
		endereco.setRua(null);
		EnderecoBC.getInstance().insert(endereco);
	}
	
	/**
	 * Valida o endereco sem o numero
	 * Como aqui numero eh um tipo primitivo sera setado o 0, indicando que
	 * este nao foi modificado
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoSemNumero(){
		endereco.setNumero(null);
		EnderecoBC.getInstance().insert(endereco);
	}
	
	/**
	 * Valida o endereco com o numero negativo
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoComNumeroNegativo(){
		endereco.setNumero(-200);
		EnderecoBC.getInstance().insert(endereco);
	}
	
	/**
	 * Valida o endereco sem complemento
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoSemComplemento(){
		endereco.setComplemento(null);
		EnderecoBC.getInstance().insert(endereco);
	}
	
	/**
	 * Valida o endereco sem bairro
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoSemBairro(){
		endereco.setBairro(null);
		EnderecoBC.getInstance().insert(endereco);
	}
	
	/**
	 * Valida o endereco sem cidade
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoSemCidade(){
		endereco.setCidade(null);
		EnderecoBC.getInstance().insert(endereco);
	}
	
	/**
	 * Valida o endereco sem estado
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoSemEstado(){
		endereco.setEstado(null);
		EnderecoBC.getInstance().insert(endereco);
	}
	
	/**
	 * Valida o endereco sem pais
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoSemPais(){
		endereco.setPais(null);
		EnderecoBC.getInstance().insert(endereco);
	}
	
}
