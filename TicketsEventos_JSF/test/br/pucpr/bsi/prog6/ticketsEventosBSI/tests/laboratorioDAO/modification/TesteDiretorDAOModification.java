package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioDAO.modification;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.DiretorBC;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.DiretorEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioBC.TesteDiretorBC;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.util.Atualizador;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.util.Verificador;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

public class TesteDiretorDAOModification extends TesteDiretorBC {
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TesteDiretorDAOModification(DiretorEnum diretor) {
		super(diretor);
	}

	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Metodo responsavel por realizar um INSERT na base de Dados
	 */
	@Test
	@Override
	public void criar(){
		//Realiza o INSERT na chamada do metodo para o BC
		//DICA: Caso nao esteja rodando aqui, veja se voce esta chamando a classe DAO a partir da BC
		super.criar();
		
		//Verifica se o id eh maior que zero
		Assert.assertTrue(diretor.getId() > 0);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Verificador.verificar(DiretorBC.getInstance().findById(diretor.getId()), diretor);
	}
	
	/**
	 * Metodo responsavel por atualizar um diretor completo
	 */
	@Test
	public void updateDiretorCompleto(){
		//Realiza o INSERT na chamada do metodo para o BC
		criar();
		
		//Atualiza Atributos
		Atualizador.atualizar(diretor, "U-");
		
		//Atualiza o objeto no BD
		DiretorBC.getInstance().update(diretor);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Verificador.verificar(DiretorBC.getInstance().findById(diretor.getId()), diretor);
		
		//Remove o objeto do BD - Para nao dar problemas no teste de queries
		DiretorBC.getInstance().delete(diretor);
	}
	
	/**
	 * Metodo responsavel por deletar um diretor completo
	 */
	@Test
	public void deletarDiretorCompleto(){
		//Realiza o INSERT na chamada do metodo para o BC
		criar();

		//Remove o objeto do BD
		DiretorBC.getInstance().delete(diretor);
		
		//Verifica se o objeto deixou de existir no BD
		Assert.assertNull(DiretorBC.getInstance().findById(diretor.getId()));
	}
}
