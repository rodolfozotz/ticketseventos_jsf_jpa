package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums;


public enum VendaEnum {
	
	VENDA_FILME_01("Reservado", PessoaEnum.CLIENTE_01, EventoEnum.FILME_01, EventoEnum.FILME_02),
	VENDA_FILME_02("Reservado", PessoaEnum.CLIENTE_02, EventoEnum.FILME_03, EventoEnum.FILME_04, EventoEnum.FILME_05),
	VENDA_FILME_03("Reservado", PessoaEnum.CLIENTE_03, EventoEnum.FILME_06),
	VENDA_FILME_04("Vendido",   PessoaEnum.CLIENTE_04, EventoEnum.FILME_01, EventoEnum.FILME_02),
	VENDA_FILME_05("Vendido",   PessoaEnum.CLIENTE_05, EventoEnum.FILME_03, EventoEnum.FILME_04, EventoEnum.FILME_05),
	VENDA_FILME_06("Vendido",   PessoaEnum.CLIENTE_06, EventoEnum.FILME_06),
	
	VENDA_PECA_01("Reservado", PessoaEnum.CLIENTE_01, EventoEnum.PECA_01, EventoEnum.PECA_02),
	VENDA_PECA_02("Reservado", PessoaEnum.CLIENTE_02, EventoEnum.PECA_03),
	VENDA_PECA_03("Reservado", PessoaEnum.CLIENTE_03, EventoEnum.PECA_04, EventoEnum.PECA_05, EventoEnum.PECA_06),
	VENDA_PECA_04("Vendido",   PessoaEnum.CLIENTE_04, EventoEnum.PECA_01),
	VENDA_PECA_05("Vendido",   PessoaEnum.CLIENTE_05, EventoEnum.PECA_02, EventoEnum.PECA_03),
	VENDA_PECA_06("Vendido",   PessoaEnum.CLIENTE_06, EventoEnum.PECA_04, EventoEnum.PECA_05, EventoEnum.PECA_06),
	
	VENDA_SHOW_01("Reservado", PessoaEnum.CLIENTE_01, EventoEnum.SHOW_01, EventoEnum.SHOW_02, EventoEnum.SHOW_03),
	VENDA_SHOW_02("Reservado", PessoaEnum.CLIENTE_02, EventoEnum.SHOW_04),
	VENDA_SHOW_03("Reservado", PessoaEnum.CLIENTE_03, EventoEnum.SHOW_05, EventoEnum.SHOW_06),
	VENDA_SHOW_04("Vendido",   PessoaEnum.CLIENTE_04, EventoEnum.SHOW_01, EventoEnum.SHOW_02),
	VENDA_SHOW_05("Vendido",   PessoaEnum.CLIENTE_05, EventoEnum.SHOW_03),
	VENDA_SHOW_06("Vendido",   PessoaEnum.CLIENTE_06, EventoEnum.SHOW_04, EventoEnum.SHOW_05, EventoEnum.SHOW_06);

	
	private String tipoVenda;
	private PessoaEnum pessoa;
	private EventoEnum[] filmes;

	private VendaEnum(String tipoVenda, PessoaEnum pessoa, EventoEnum... filmes) {
		this.tipoVenda = tipoVenda;
		this.pessoa = pessoa;
		this.filmes = filmes;
	}
	
	public String getTipoVenda() {
		return tipoVenda;
	}
	
	public PessoaEnum getPessoa() {
		return pessoa;
	}
	
	public EventoEnum[] getEventos() {
		return filmes;
	}
	
	public boolean isVendaCompleta(){
		return tipoVenda.equals("Vendido");
	}
}
