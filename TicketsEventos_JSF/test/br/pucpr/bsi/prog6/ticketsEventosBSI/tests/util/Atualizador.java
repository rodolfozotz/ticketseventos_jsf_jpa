package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.util;

import java.util.Date;

import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Ambiente;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Cliente;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Diretor;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Evento;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Filme;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Funcionario;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Peca;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Pessoa;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Show;
import br.pucpr.bsi.prog6.ticketsEventosBSI.utils.DateUtils;

/**
 * Classe responsavel por atualizar informacoes para update e comparacoes
 * @author Mauda
 *
 */
public class Atualizador {
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param objeto
	 * @param cod
	 */
	public static void atualizar(Ambiente objeto, String cod){
		//Atualiza o ambiente
		objeto.setDescricao(cod + objeto.getDescricao());
		objeto.setNome(cod + objeto.getNome());
		
		//Atualiza o endereco
		atualizar(objeto.getEndereco(), cod);
		
		//Atualiza os setores
		for (Setor setor : objeto.getSetores()) {
			atualizar(setor, cod);
		}
	}
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param objeto
	 * @param cod
	 */
	public static void atualizar(Artista objeto, String cod){
		objeto.setNome(cod 		+ objeto.getNome());
	}
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param objeto
	 * @param cod
	 */
	public static void atualizar(Diretor objeto, String cod){
		objeto.setNome(cod 		+ objeto.getNome());
	}
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param objeto
	 * @param cod
	 */
	public static void atualizar(Endereco objeto, String cod){
		//Atualiza o endereco
		objeto.setBairro(cod + objeto.getBairro());
		objeto.setCidade(cod + objeto.getCidade());
		objeto.setComplemento(cod + objeto.getComplemento());
		objeto.setEstado(cod + objeto.getEstado());
		objeto.setNumero(100000);
		objeto.setPais(cod + objeto.getPais());
		objeto.setRua(cod + objeto.getRua());
	}
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param objeto
	 * @param cod
	 */
	public static void atualizar(Evento objeto, String cod){
		//Atualiza o evento
		objeto.setCensura(80);
		objeto.setDataEvento(		DateUtils.datePlusDays(new Date(), 365));
		objeto.setDescricao(cod		+ objeto.getDescricao());
		objeto.setNome(cod			+ objeto.getNome());
		
		switch (objeto.getTipoEventoEnum()) {
		case FILME:
			((Filme)objeto).setGenero(cod	+ ((Filme)objeto).getGenero());
			break;
		case PECA:
			((Peca)objeto).setGenero(cod	+ ((Peca)objeto).getGenero());
			((Peca)objeto).setCompanhia(cod + ((Peca)objeto).getCompanhia());
			break;
		case SHOW:
			((Show)objeto).setEstilo(cod	+ ((Show)objeto).getEstilo());
		}
	}
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param objeto
	 * @param cod
	 */
	public static void atualizar(Pessoa objeto, String cod){
		//Atualiza a pessoa
		objeto.setDataNascimento(	DateUtils.datePlusDays(new Date(), -365));
		objeto.setEmail(cod 		+ objeto.getEmail());
		objeto.setNome(cod 		+ objeto.getNome());
		objeto.setSenha(cod 		+ objeto.getSenha());
		objeto.setTelefone(cod 	+ objeto.getTelefone());
		objeto.setUsuario(cod 		+ objeto.getUsuario());

		//Atualiza o endereco
		atualizar(objeto.getEndereco(), cod);
		
		//Atualizar atributos especificos
		switch (objeto.getTipoPessoaEnum()) {
		case CLIENTE:
			Cliente cliente = ((Cliente)objeto); 
			cliente.setDocumento(cod + cliente.getDocumento());	
			cliente.setNumeroCartao(cod + cliente.getNumeroCartao());
			break;
		case FUNCIONARIO:
			Funcionario funcionario = ((Funcionario)objeto); 
			funcionario.setCodigo(cod + funcionario.getCodigo());
			funcionario.setContaCorrente(cod + funcionario.getContaCorrente());
		}
		
	}
	
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param objeto
	 * @param cod
	 */
	public static void atualizar(Setor objeto, String cod){ 
		//Atualiza o setor
		objeto.setCapacidade(50);
		objeto.setNome(cod + objeto.getNome());
		objeto.setPreco(500.00);
	}
}
