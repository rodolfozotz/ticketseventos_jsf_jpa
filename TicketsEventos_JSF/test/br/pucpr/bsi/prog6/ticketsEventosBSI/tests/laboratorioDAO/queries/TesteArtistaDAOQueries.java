package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioDAO.queries;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.ArtistaBC;
import br.pucpr.bsi.prog6.ticketsEventosBSI.exception.BSIException;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.ArtistaEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.util.Verificador;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * IMPORTANTE: Para rodar esses teste, limpar a base do Banco de Dados antes e rodar os testes da camada BC.
 * 
 * @author Mauda
 * 
 */

public class TesteArtistaDAOQueries {
	
	///////////////////////////////////////////////////////////
	// TESTE DOS METODOS findAll E findById 
	///////////////////////////////////////////////////////////
	
	@Test
	public void testFindAlleFindById(){
		List<Artista> listaBD = ArtistaBC.getInstance().findAll();
		
		//Verifica se a quantidade de instancias a mesma dos testes de populate
		Assert.assertEquals(listaBD.size(), ArtistaEnum.values().length);
		
		for (Artista artistaFindAll : listaBD) {
			Artista artistaFindId = ArtistaBC.getInstance().findById(artistaFindAll.getId());
			Verificador.verificar(artistaFindAll, artistaFindId);
		}
		
		//buscando um id inexistente
		Artista artistaIdInvalido = ArtistaBC.getInstance().findById(100000L);
		Assert.assertNull(artistaIdInvalido);
	}
	
	///////////////////////////////////////////////////////////
	// TESTES DO METODO findByFilter 
	///////////////////////////////////////////////////////////	
		
	@Test(expected = BSIException.class)
	public void testFindByFilterNulo(){
		ArtistaBC.getInstance().findByFilter(null);
	}	
	
	@Test(expected = BSIException.class)
	public void testFindByFilterVazio(){
		ArtistaBC.getInstance().findByFilter(new Artista());
	}	
	
	@Test
	public void testFindByFilterNome(){
		Artista filter = new Artista();
		for (ArtistaEnum artistaEnum : ArtistaEnum.values()) {
			filter.setNome(artistaEnum.getNome());
			List<Artista> artistasFilter = ArtistaBC.getInstance().findByFilter(filter);
			
			//verifica se o tamanho do filtro eh 1
			Assert.assertEquals(artistasFilter.size(), 1);
			
			//Nomes sao iguais
			Assert.assertEquals(artistasFilter.get(0).getNome(), artistaEnum.getNome());
		}
	}
	
}
