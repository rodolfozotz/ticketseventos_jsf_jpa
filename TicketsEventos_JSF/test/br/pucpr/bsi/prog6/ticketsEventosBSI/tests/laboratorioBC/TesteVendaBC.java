package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioBC;

import org.junit.Test;

import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.VendaBC;
import br.pucpr.bsi.prog6.ticketsEventosBSI.exception.BSIException;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Ingresso;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.VendaEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioModel.TesteVendaModel;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

public class TesteVendaBC extends TesteVendaModel{
	
	///////////////////////////////////////////
	// CONSTRUTORES
	///////////////////////////////////////////
	
	public TesteVendaBC(VendaEnum venda) {
		super(venda);
	}
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Metodo responsavel por criar uma venda completa
	 */
	@Test
	@Override
	public void criar(){
		super.criar();
		VendaBC.getInstance().reservar(venda);
	}
	
	/**
	 * Valida a venda nula
	 */
	@Test(expected = BSIException.class)
	public void validarVendaNula(){
		VendaBC.getInstance().reservar(null);
	}
	
	/**
	 * Valida a venda sem ingressos
	 */
	@Test(expected = BSIException.class)
	public void validarVendaSemIngressos(){
		venda.getIngressos().clear();
		VendaBC.getInstance().reservar(venda);
	}	
	
	/**
	 * Valida a venda sem ingressos
	 */
	@Test(expected = BSIException.class)
	public void validarVendaComIngressoNulo(){
		venda.getIngressos().clear();
		venda.getIngressos().add(null);
		VendaBC.getInstance().reservar(venda);
	}
	
	/**
	 * Valida a venda com ingresso sem venda
	 */
	@Test(expected = BSIException.class)
	public void validarEventoIngressoSemVenda(){
		for(Ingresso ingresso : venda.getIngressos()){
			ingresso.setVenda(null);
		}
		VendaBC.getInstance().reservar(venda);
	}	
	
	/**
	 * Valida a venda sem pessoa
	 */
	@Test(expected = BSIException.class)
	public void validarVendaSemPessoa(){
		venda.setCliente(null);
		VendaBC.getInstance().reservar(venda);
	}
}
