package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums;

import java.util.Random;

public enum AmbienteEnum {
	
	CASA_SHOW_01("Casa Show 01", "Casa Show 01", 'S', EnderecoEnum.getEndereco(), SetorEnum.PISTA_A, SetorEnum.CAMAROTE_A),
	CASA_SHOW_02("Casa Show 02", "Casa Show 02", 'S', EnderecoEnum.getEndereco(), SetorEnum.PISTA_B, SetorEnum.CAMAROTE_B),
	CASA_SHOW_03("Casa Show 03", "Casa Show 03", 'S', EnderecoEnum.getEndereco(), SetorEnum.PISTA_C, SetorEnum.CAMAROTE_C),
	
	CINEMA_01("Cinema 01", "Cinema 01", 'C', EnderecoEnum.getEndereco(), SetorEnum.PISTA_A),
	CINEMA_02("Cinema 02", "Cinema 02", 'C', EnderecoEnum.getEndereco(), SetorEnum.PISTA_B),
	CINEMA_03("Cinema 03", "Cinema 03", 'C', EnderecoEnum.getEndereco(), SetorEnum.PISTA_C),
			
	TEATRO_01("Teatro 01", "Teatro 01", 'T', EnderecoEnum.getEndereco(), SetorEnum.PISTA_A, SetorEnum.CAMAROTE_A, SetorEnum.CAMAROTE_B),
	TEATRO_02("Teatro 02", "Teatro 02", 'T', EnderecoEnum.getEndereco(), SetorEnum.PISTA_B, SetorEnum.CAMAROTE_B, SetorEnum.CAMAROTE_C),
	TEATRO_03("Teatro 03", "Teatro 03", 'T', EnderecoEnum.getEndereco(), SetorEnum.PISTA_C, SetorEnum.CAMAROTE_C, SetorEnum.CAMAROTE_A);
	
	private String nome, descricao;
	private char tipoAmbiente;
	private EnderecoEnum enderecoEnum;
	private SetorEnum[] setores;
	
	private AmbienteEnum(String nome, String descricao, char tipoAmbiente, EnderecoEnum enderecoEnum, SetorEnum... setores) {
		this.nome = nome;
		this.descricao = descricao;
		this.tipoAmbiente = tipoAmbiente;
		this.enderecoEnum = enderecoEnum;
		this.setores = setores;
	}
	
	public String getNome() {
		return nome;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public char getTipoAmbiente() {
		return tipoAmbiente;
	}
	
	public EnderecoEnum getEnderecoEnum() {
		return enderecoEnum;
	}
	
	public SetorEnum[] getSetores() {
		return setores;
	}
	
	public int getCapacidade(){
		int capacidade = 0;
		for (SetorEnum setor : setores) {
			capacidade += setor.getCapacidade();
		}
		return capacidade;
	}
	
	public static AmbienteEnum getAmbiente(){
		Random generator = new Random();
		AmbienteEnum[] array = values();
		int length = array.length;
		return array[generator.nextInt(length)];
	}
	
}
