package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioDAO.modification;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.PessoaBCFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Pessoa;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.PessoaEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioBC.TestePessoaBC;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.util.Atualizador;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.util.Verificador;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

@SuppressWarnings("unchecked")
public class TestePessoaDAOModification extends TestePessoaBC {
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TestePessoaDAOModification(PessoaEnum pessoa) {
		super(pessoa);
	}

	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Metodo responsavel por realizar um INSERT na base de Dados
	 */
	@Test
	@Override
	public void criar(){
		//Realiza o INSERT na chamada do metodo para o BC
		//DICA: Caso não esteja rodando aqui, veja se voce esta chamando a classe DAO a partir da BC
		super.criar();
		
		//Verifica se o id eh maior que zero
		Assert.assertTrue(pessoa.getId() > 0);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Verificador.verificar((Pessoa)PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).findById(pessoa.getId()), pessoa);
	}
	
	/**
	 * Metodo responsavel por atualizar um pessoa completo
	 */
	@Test
	public void updatePessoaCompleto(){
		//Realiza o INSERT na chamada do metodo para o BC
		criar();
		
		//Atualiza Atributos
		Atualizador.atualizar(pessoa, "U-");
		
		//Atualiza o objeto no BD
		PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).update(pessoa);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Verificador.verificar((Pessoa)PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).findById(pessoa.getId()), pessoa);
		
		//Remove o objeto do BD - Para nao dar problemas no teste de queries
		PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).delete(pessoa);
	}
	
	/**
	 * Metodo responsavel por deletar um pessoa completo
	 */
	@Test
	public void deletarPessoaCompleto(){
		//Realiza o INSERT na chamada do metodo para o BC
		criar();

		//Remove o objeto do BD
		PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).delete(pessoa);
		
		//Verifica se o objeto deixou de existir no BD
		Assert.assertNull((Pessoa)PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).findById(pessoa.getId()));
	}
}
