package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioBC;

import java.util.Date;

import org.junit.Test;

import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.PessoaBCFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.exception.BSIException;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Cliente;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Funcionario;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.PessoaEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioModel.TestePessoaModel;
import br.pucpr.bsi.prog6.ticketsEventosBSI.utils.DateUtils;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */

@SuppressWarnings("unchecked")
public class TestePessoaBC extends TestePessoaModel {
	
	///////////////////////////////////////////
	// CONSTRUTORES
	///////////////////////////////////////////
	
	public TestePessoaBC(PessoaEnum pessoaEnum) {
		super(pessoaEnum);
	}
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Metodo responsavel por criar uma pessoa completa
	 */
	@Test
	@Override
	public void criar(){
		super.criar();
		PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).insert(pessoa);
	}
	
	/**
	 * Valida uma pessoa nula
	 */
	@Test(expected = BSIException.class)
	public void validarPessoaNula(){
		PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).insert(null);
	}
	
	/**
	 * Valida uma pessoa com endereco nulo
	 */
	@Test(expected = BSIException.class)
	public void validarEnderecoNulo(){
		pessoa.setEndereco(null);
		PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).insert(pessoa);
	}	
	
	/**
	 * Valida uma pessoa preenchida com espacos em branco
	 */
	@Test(expected = BSIException.class)
	public void validarPessoaEspacosBranco(){
		pessoa.setNome("                              ");
		pessoa.setEmail("                             ");
		pessoa.setTelefone("                          ");
		pessoa.setUsuario("                             ");
		pessoa.setSenha("                             ");
		
		PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).insert(pessoa);
	}

	/**
	 * Valida uma pessoa sem nome
	 */
	@Test(expected = BSIException.class)
	public void validarPessoaSemNome(){
		pessoa.setNome(null);
		PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).insert(pessoa);
	}	
	
	/**
	 * Valida uma pessoa sem email
	 */
	@Test(expected = BSIException.class)
	public void validarPessoaSemEmail(){
		pessoa.setEmail(null);
		PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).insert(pessoa);
	}
			
	/**
	 * Valida uma pessoa sem @ no email
	 */
	@Test(expected = BSIException.class)
	public void validarPessoaComEmailSemArroba(){
		pessoa.setEmail("func1ticketeventosbsi.com.br");
		PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).insert(pessoa);
	}
	
	/**
	 * Valida uma pessoa sem . apos o @ no email
	 */
	@Test(expected = BSIException.class)
	public void validarPessoaComEmailSemPonto(){
		pessoa.setEmail("func.1@ticketeventosbsi");
		PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).insert(pessoa);
	}
	
	/**
	 * Valida uma pessoa sem telefone
	 */
	@Test(expected = BSIException.class)
	public void validarPessoaSemTelefone(){
		pessoa.setTelefone(null);
		PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).insert(pessoa);
	}	
	
	/**
	 * Valida uma pessoa sem data nascimento
	 */
	@Test(expected = BSIException.class)
	public void validarPessoaSemDataNascimento(){
		pessoa.setDataNascimento(null);
		PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).insert(pessoa);
	}
	
	/**
	 * Valida uma pessoa com data nascimento 1 ano apos a data atual
	 */
	@Test(expected = BSIException.class)
	public void validarPessoaComDataNascimentoAposDataAtual(){
		pessoa.setDataNascimento(DateUtils.datePlusDays(new Date(), 365));
		PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).insert(pessoa);
	}
	
	/**
	 * Valida uma pessoa sem senha
	 */
	@Test(expected = BSIException.class)
	public void validarPessoaSemSenha(){
		pessoa.setSenha(null);
		PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).insert(pessoa);
	}
	
	/**
	 * Valida uma pessoa sem user
	 */
	@Test(expected = BSIException.class)
	public void validarPessoaSemUsuario(){
		pessoa.setUsuario(null);
		PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).insert(pessoa);
	}
	
	/**
	 * Valida uma pessoa sem documento
	 */
	@Test(expected = BSIException.class)
	public void validarPessoaSemDocumento(){
		switch(pessoa.getTipoPessoaEnum()){
		case CLIENTE:
			((Cliente)pessoa).setDocumento(null);
			break;
		case FUNCIONARIO:
			throw new BSIException("N�o � poss�vel testar");
		}
		PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).insert(pessoa);
	}
	
	/**
	 * Valida uma pessoa sem numero cartao
	 */
	@Test(expected = BSIException.class)
	public void validarPessoaSemNumeroCartao(){
		switch(pessoa.getTipoPessoaEnum()){
		case CLIENTE:
			((Cliente)pessoa).setNumeroCartao(null);
			break;
		case FUNCIONARIO:
			throw new BSIException("N�o � poss�vel testar");
		}
		PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).insert(pessoa);
	}
	
	/**
	 * Valida uma pessoa sem codigo
	 */
	@Test(expected = BSIException.class)
	public void validarPessoaSemCodigo(){
		switch(pessoa.getTipoPessoaEnum()){
		case CLIENTE:
			throw new BSIException("N�o � poss�vel testar");
		case FUNCIONARIO:
			((Funcionario)pessoa).setCodigo(null);
		}
		PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).insert(pessoa);
	}
	
	/**
	 * Valida uma pessoa sem conta corrente
	 */
	@Test(expected = BSIException.class)
	public void validarPessoaSemContaCorrente(){
		switch(pessoa.getTipoPessoaEnum()){
		case CLIENTE:
			throw new BSIException("N�o � poss�vel testar");
		case FUNCIONARIO:
			((Funcionario)pessoa).setContaCorrente(null);
		}
		PessoaBCFactory.getInstance(pessoa.getTipoPessoaEnum()).insert(pessoa);
	}
}