package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums;

import java.util.Random;

public enum DiretorEnum{
	
	HECTOR_BABENCO("Hector Babenco"),
	LLOYD_BACON("Lloyd Bacon"),
	CLARENCE_BADGER("Clarence Badger"),
	JOHN_BADHAM("John Badham"),
	BAE_YONG_KYUN("Bae Yong-Kyun"),
	KING_BAGGOT("King Baggot"),
	ROY_WARD_BAKER("Roy Ward Baker");
	
	private String nome;
	
	private DiretorEnum(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
	
	public static DiretorEnum getDiretor(){
		Random generator = new Random();
		DiretorEnum[] array = values();
		int length = array.length;
		return array[generator.nextInt(length)];
	}
}
