package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioDAO.queries;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.AmbienteBCFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.exception.BSIException;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Ambiente;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.AmbienteFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.enums.TipoAmbienteEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.AmbienteEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EnderecoEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.SetorEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.util.Verificador;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * IMPORTANTE: Para rodar esses teste, limpar a base do Banco de Dados antes e rodar os testes da camada BC.
 * 
 * @author Mauda
 * 
 */

@SuppressWarnings("unchecked")
public class TesteAmbienteDAOQueries {
	
	///////////////////////////////////////////////////////////
	// TESTE DOS METODOS findAll E findById 
	///////////////////////////////////////////////////////////
	
	@Test
	public void testFindAlleFindById(){
		List<Ambiente> listaBD = AmbienteBCFactory.getInstance(TipoAmbienteEnum.CASASHOW).findAll();
		listaBD.addAll(AmbienteBCFactory.getInstance(TipoAmbienteEnum.CINEMA).findAll());
		listaBD.addAll(AmbienteBCFactory.getInstance(TipoAmbienteEnum.TEATRO).findAll());
		
		//Verifica se a quantidade de instancias a mesma dos testes de populate
		Assert.assertEquals(listaBD.size(), AmbienteEnum.values().length);
		
		for (Ambiente ambienteFindAll : listaBD) {
			Ambiente ambienteFindId = (Ambiente) AmbienteBCFactory.getInstance(ambienteFindAll.getTipoAmbienteEnum()).findById(ambienteFindAll.getId());
			Verificador.verificar(ambienteFindAll, ambienteFindId);
		}
		
		//buscando um id inexistente
		Ambiente ambienteIdInvalido = (Ambiente) AmbienteBCFactory.getInstance(TipoAmbienteEnum.CINEMA).findById(100000L);
		Assert.assertNull(ambienteIdInvalido);
	}
	
	///////////////////////////////////////////////////////////
	// TESTES DO METODO findByFilter 
	///////////////////////////////////////////////////////////	
		
	@Test(expected = BSIException.class)
	public void testFindByFilterNulo(){
		AmbienteBCFactory.getInstance(TipoAmbienteEnum.TEATRO).findByFilter(null);
	}	
	
	@Test
	public void testFindByFilterNome(){
		for (AmbienteEnum ambienteEnum : AmbienteEnum.values()) {
			Ambiente filter = AmbienteFactory.getInstance(TipoAmbienteEnum.getBy(ambienteEnum.getTipoAmbiente()), new Endereco()); 
			filter.setNome(ambienteEnum.getNome());
			List<Ambiente> ambientesFilter = AmbienteBCFactory.getInstance(TipoAmbienteEnum.getBy(ambienteEnum.getTipoAmbiente())).findByFilter(filter);
			
			//verifica se o tamanho do filtro eh 1
			Assert.assertEquals(ambientesFilter.size(), 1);
			
			//Nomes sao iguais
			Assert.assertEquals(ambientesFilter.get(0).getNome(), ambienteEnum.getNome());
		}
	}
	
	@Test
	public void testFindByFilterDescricao(){
		for (AmbienteEnum ambienteEnum : AmbienteEnum.values()) {
			Ambiente filter = AmbienteFactory.getInstance(TipoAmbienteEnum.getBy(ambienteEnum.getTipoAmbiente()), new Endereco());
			filter.setDescricao(ambienteEnum.getDescricao());
			List<Ambiente> ambientesFilter = AmbienteBCFactory.getInstance(TipoAmbienteEnum.getBy(ambienteEnum.getTipoAmbiente())).findByFilter(filter);
			
			//verifica se o tamanho do filtro eh 1
			Assert.assertEquals(ambientesFilter.size(), 1);
			
			//Descricoes sao iguais
			Assert.assertEquals(ambientesFilter.get(0).getDescricao(), ambienteEnum.getDescricao());
		}
	}
	
	///////////////////////////////////////////////////////////
	// DADOS DO SETOR
	///////////////////////////////////////////////////////////
	
	@Test
	public void testFindByFilterNomeSetor(){
		Ambiente filter = AmbienteFactory.getInstance(TipoAmbienteEnum.TEATRO, new Endereco());
		for (SetorEnum setorEnum : SetorEnum.values()) {
			filter.getSetores().add(new Setor(filter));
			filter.getSetores().get(0).setNome(setorEnum.getNome());
			List<Ambiente> ambientesFilter = AmbienteBCFactory.getInstance(filter.getTipoAmbienteEnum()).findByFilter(filter);
			
			boolean existeSetor = false;
			for(Setor setor : ambientesFilter.get(0).getSetores()){
				if(setorEnum.getNome().equals(setor.getNome())){
					existeSetor = true;
					break;
				}
			}
			Assert.assertTrue(existeSetor);
		}
	}
	
	@Test
	public void testFindByFilterCapacidadeSetor(){
		Ambiente filter = AmbienteFactory.getInstance(TipoAmbienteEnum.TEATRO, new Endereco());
		for (SetorEnum setorEnum : SetorEnum.values()) {
			filter.getSetores().add(new Setor(filter));
			filter.getSetores().get(0).setCapacidade(setorEnum.getCapacidade());
			List<Ambiente> ambientesFilter = AmbienteBCFactory.getInstance(filter.getTipoAmbienteEnum()).findByFilter(filter);
			
			boolean existeSetor = false;
			for(Ambiente ambiente : ambientesFilter){
				for(Setor setor : ambiente.getSetores()){
					if(setorEnum.getCapacidade().equals(setor.getCapacidade())){
						existeSetor = true;
						break;
					}
				}
			}
			Assert.assertTrue(existeSetor);
		}
	}
	
	@Test
	public void testFindByFilterPrecoSetor(){
		Ambiente filter = AmbienteFactory.getInstance(TipoAmbienteEnum.TEATRO, new Endereco());
		for (SetorEnum setorEnum : SetorEnum.values()) {
			filter.getSetores().add(new Setor(filter));
			filter.getSetores().get(0).setPreco(setorEnum.getPreco());
			List<Ambiente> ambientesFilter = AmbienteBCFactory.getInstance(filter.getTipoAmbienteEnum()).findByFilter(filter);
			
			boolean existeSetor = false;
			for(Ambiente ambiente : ambientesFilter){
				for(Setor setor : ambiente.getSetores()){
					if(setorEnum.getPreco().equals(setor.getPreco())){
						existeSetor = true;
						break;
					}
				}
			}
			Assert.assertTrue(existeSetor);
		}
	}

	
	///////////////////////////////////////////////////////////
	// DADOS DO ENDERECO
	///////////////////////////////////////////////////////////
	
	@Test
	public void testFindByFilterRuaEndereco(){
		Ambiente filter = AmbienteFactory.getInstance(TipoAmbienteEnum.CINEMA, new Endereco());
		for (EnderecoEnum enderecoEnum : EnderecoEnum.values()) {
			filter.getEndereco().setRua(enderecoEnum.getRua());
			List<Ambiente> ambienteFilter = AmbienteBCFactory.getInstance(filter.getTipoAmbienteEnum()).findByFilter(filter);
			
			for (Ambiente ambiente : ambienteFilter) {
				Assert.assertEquals(ambiente.getEndereco().getRua(), enderecoEnum.getRua());				
			}
		}
	}	
	
	@Test
	public void testFindByFilterNumeroEndereco(){
		Ambiente filter = AmbienteFactory.getInstance(TipoAmbienteEnum.CINEMA, new Endereco());
		for (EnderecoEnum enderecoEnum : EnderecoEnum.values()) {
			filter.getEndereco().setNumero(enderecoEnum.getNumero());
			List<Ambiente> ambienteFilter = AmbienteBCFactory.getInstance(filter.getTipoAmbienteEnum()).findByFilter(filter);
			
			for (Ambiente ambiente : ambienteFilter) {
				Assert.assertEquals(ambiente.getEndereco().getNumero(), enderecoEnum.getNumero(), 0);				
			}
		}
	}	
	
	@Test
	public void testFindByFilterComplementoEndereco(){
		Ambiente filter = AmbienteFactory.getInstance(TipoAmbienteEnum.CINEMA, new Endereco());
		for (EnderecoEnum enderecoEnum : EnderecoEnum.values()) {
			filter.getEndereco().setComplemento(enderecoEnum.getComplemento());
			List<Ambiente> ambienteFilter = AmbienteBCFactory.getInstance(filter.getTipoAmbienteEnum()).findByFilter(filter);
			
			for (Ambiente ambiente : ambienteFilter) {
				Assert.assertEquals(ambiente.getEndereco().getComplemento(), enderecoEnum.getComplemento());				
			}
		}
	}	
	
	@Test
	public void testFindByFilterBairroEndereco(){
		Ambiente filter = AmbienteFactory.getInstance(TipoAmbienteEnum.CINEMA, new Endereco());
		for (EnderecoEnum enderecoEnum : EnderecoEnum.values()) {
			filter.getEndereco().setBairro(enderecoEnum.getBairro());
			List<Ambiente> ambienteFilter = AmbienteBCFactory.getInstance(filter.getTipoAmbienteEnum()).findByFilter(filter);
			
			for (Ambiente ambiente : ambienteFilter) {
				Assert.assertEquals(ambiente.getEndereco().getBairro(), enderecoEnum.getBairro());				
			}
		}
	}
	
	@Test
	public void testFindByFilterCidadeEndereco(){
		Ambiente filter = AmbienteFactory.getInstance(TipoAmbienteEnum.CINEMA, new Endereco());
		for (EnderecoEnum enderecoEnum : EnderecoEnum.values()) {
			filter.getEndereco().setCidade(enderecoEnum.getCidade());
			List<Ambiente> ambienteFilter = AmbienteBCFactory.getInstance(filter.getTipoAmbienteEnum()).findByFilter(filter);
			
			for (Ambiente ambiente : ambienteFilter) {
				Assert.assertEquals(ambiente.getEndereco().getCidade(), enderecoEnum.getCidade());				
			}
		}
	}
	
	@Test
	public void testFindByFilterEstadoEndereco(){
		Ambiente filter = AmbienteFactory.getInstance(TipoAmbienteEnum.CINEMA, new Endereco());
		for (EnderecoEnum enderecoEnum : EnderecoEnum.values()) {
			filter.getEndereco().setEstado(enderecoEnum.getEstado());
			List<Ambiente> ambienteFilter = AmbienteBCFactory.getInstance(filter.getTipoAmbienteEnum()).findByFilter(filter);
			
			for (Ambiente ambiente : ambienteFilter) {
				Assert.assertEquals(ambiente.getEndereco().getEstado(), enderecoEnum.getEstado());				
			}
		}
	}
	
	@Test
	public void testFindByFilterPaisEndereco(){
		Ambiente filter = AmbienteFactory.getInstance(TipoAmbienteEnum.CINEMA, new Endereco());
		for (EnderecoEnum enderecoEnum : EnderecoEnum.values()) {
			filter.getEndereco().setPais(enderecoEnum.getPais());
			List<Ambiente> ambienteFilter = AmbienteBCFactory.getInstance(filter.getTipoAmbienteEnum()).findByFilter(filter);
			
			for (Ambiente ambiente : ambienteFilter) {
				Assert.assertEquals(ambiente.getEndereco().getPais(), enderecoEnum.getPais());				
			}
		}
	}
	
	
}
