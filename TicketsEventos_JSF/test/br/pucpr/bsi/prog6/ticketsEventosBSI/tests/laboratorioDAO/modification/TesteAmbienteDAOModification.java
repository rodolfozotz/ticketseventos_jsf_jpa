package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioDAO.modification;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.AmbienteBCFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Ambiente;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.AmbienteEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioBC.TesteAmbienteBC;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.util.Atualizador;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.util.Verificador;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

@SuppressWarnings("unchecked")
public class TesteAmbienteDAOModification extends TesteAmbienteBC {
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TesteAmbienteDAOModification(AmbienteEnum ambiente) {
		super(ambiente);
	}

	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Metodo responsavel por realizar um INSERT na base de Dados
	 */
	@Test
	@Override
	public void criar(){
		//Realiza o INSERT na chamada do metodo para o BC
		//DICA: Caso não esteja rodando aqui, veja se voce esta chamando a classe DAO a partir da BC
		super.criar();
		
		//Verifica se o id eh maior que zero
		Assert.assertTrue(ambiente.getId() > 0);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Verificador.verificar((Ambiente)AmbienteBCFactory.getInstance(ambiente.getTipoAmbienteEnum()).findById(ambiente.getId()), ambiente);
	}
	
	/**
	 * Metodo responsavel por atualizar um ambiente completo
	 */
	@Test
	public void updateAmbienteCompleto(){
		//Realiza o INSERT na chamada do metodo para o BC
		criar();
		
		//Atualiza Atributos
		Atualizador.atualizar(ambiente, "U-");
		
		//Atualiza o objeto no BD
		AmbienteBCFactory.getInstance(ambiente.getTipoAmbienteEnum()).update(ambiente);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Verificador.verificar((Ambiente)AmbienteBCFactory.getInstance(ambiente.getTipoAmbienteEnum()).findById(ambiente.getId()), ambiente);
		
		//Remove o objeto do BD - Para nao dar problemas no teste de queries
		AmbienteBCFactory.getInstance(ambiente.getTipoAmbienteEnum()).delete(ambiente);
	}
	
	/**
	 * Metodo responsavel por deletar um ambiente completo
	 */
	@Test
	public void deletarAmbienteCompleto(){
		//Realiza o INSERT na chamada do metodo para o BC
		criar();

		//Remove o objeto do BD
		AmbienteBCFactory.getInstance(ambiente.getTipoAmbienteEnum()).delete(ambiente);
		
		//Verifica se o objeto deixou de existir no BD
		Assert.assertNull(AmbienteBCFactory.getInstance(ambiente.getTipoAmbienteEnum()).findById(ambiente.getId()));
	}
}
