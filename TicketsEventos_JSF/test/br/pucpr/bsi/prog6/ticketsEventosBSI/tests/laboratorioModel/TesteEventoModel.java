package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioModel;

import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum.FILME_01;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum.FILME_02;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum.FILME_03;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum.FILME_04;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum.FILME_05;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum.FILME_06;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum.PECA_01;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum.PECA_02;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum.PECA_03;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum.PECA_04;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum.PECA_05;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum.PECA_06;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum.SHOW_01;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum.SHOW_02;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum.SHOW_03;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum.SHOW_04;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum.SHOW_05;
import static br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum.SHOW_06;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Evento;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.EventoFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Filme;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Ingresso;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Peca;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Show;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.enums.TipoEventoEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.ArtistaEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

@RunWith(Parameterized.class)
public class TesteEventoModel {

	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////		
	protected EventoEnum eventoEnum;
	protected Evento evento;

	@SuppressWarnings("rawtypes")
	@Parameterized.Parameters
	public static Collection listaObjetos() {
		return Arrays.asList(new Object[][] {
				{ FILME_01 },
				{ FILME_02 },
				{ FILME_03 },
				{ FILME_04 },
				{ FILME_05 },
				{ FILME_06 },
				
				{ PECA_01 },
				{ PECA_02 },
				{ PECA_03 },
				{ PECA_04 },
				{ PECA_05 },
				{ PECA_06 },
				
				{ SHOW_01 },
				{ SHOW_02 },
				{ SHOW_03 },
				{ SHOW_04 },
				{ SHOW_05 },
				{ SHOW_06 }
		});
	}	
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TesteEventoModel(EventoEnum eventoEnum) {
		this.eventoEnum = eventoEnum;
	}
	
	
	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////	
	
	/**
	 * Metodo para criar um novo Evento
	 * @return
	 */
	public static Evento novoEvento(EventoEnum eventoEnum){
		
		//Para obter os artistas
		ArtistaEnum[] arrayArtistas = eventoEnum.getArtistasEnum();
		List<Artista> artistas = new ArrayList<Artista>();
		for (ArtistaEnum artistaEnum : arrayArtistas) {
			artistas.add(TesteArtistaModel.novoArtista(artistaEnum));
		}
		
		Evento evento;
		if(artistas.size() == 1){
			evento = EventoFactory.getInstance(TipoEventoEnum.getBy(eventoEnum.getTipo()),
					TesteAmbienteModel.novoAmbiente(eventoEnum.getAmbienteEnum()), 
					artistas.get(0), 
					TesteDiretorModel.novoDiretor(eventoEnum.getDiretorEnum()),
					eventoEnum.getComissao());
		} else {
			evento = EventoFactory.getInstance(TipoEventoEnum.getBy(eventoEnum.getTipo()),
					TesteAmbienteModel.novoAmbiente(eventoEnum.getAmbienteEnum()), 
					artistas, 
					TesteDiretorModel.novoDiretor(eventoEnum.getDiretorEnum()),
					eventoEnum.getComissao());
		}
		
		evento.setCensura(eventoEnum.getCensura());
		evento.setDataEvento(eventoEnum.getDataEvento());
		evento.setDescricao(eventoEnum.getDescricao());
		evento.setNome(eventoEnum.getNome());
		
		mudarAtributosEspecificos(eventoEnum, evento);
		
		return evento;
	}
	
	public static void mudarAtributosEspecificos(EventoEnum eventoEnum, Evento evento){
			switch (evento.getTipoEventoEnum()) {
			case FILME:
				((Filme)evento).setGenero(eventoEnum.getGenero());
				break;
			case PECA:
				((Peca)evento).setCompanhia(eventoEnum.getCompanhia());
				((Peca)evento).setGenero(eventoEnum.getGenero());
				break;
			case SHOW:
				((Show)evento).setEstilo(eventoEnum.getEstilo());
		}		
	}
	
	@Before
	public void criaEvento(){
		this.evento = novoEvento(this.eventoEnum);
	}
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////
	
	@Test
	public void criar() {
		//Verifica se nao eh nulo
		Assert.assertNotNull(evento);
		
		//Verifica se os atributos estao preenchidos
		Assert.assertTrue(StringUtils.isNotBlank(evento.getDescricao()));
		Assert.assertTrue(StringUtils.isNotBlank(evento.getNome()));
		Assert.assertNotNull(evento.getAmbiente());
		Assert.assertNotNull(evento.getArtistas());
		Assert.assertNotNull(evento.getCensura());
		Assert.assertNotNull(evento.getDataEvento());
		Assert.assertNotNull(evento.getDiretor());
		Assert.assertNotNull(evento.getTipoEventoEnum());
		
		switch (evento.getTipoEventoEnum()) {
			case FILME:
				Assert.assertTrue(StringUtils.isNotBlank(((Filme)evento).getGenero()));
				break;
			case PECA:
				Assert.assertTrue(StringUtils.isNotBlank(((Peca)evento).getCompanhia()));
				Assert.assertTrue(StringUtils.isNotBlank(((Peca)evento).getGenero()));
				break;
			case SHOW:
				Assert.assertTrue(StringUtils.isNotBlank(((Show)evento).getEstilo()));
		}		
		
		
		//Verifica se o ambiente possui o evento
		Assert.assertTrue(evento.getAmbiente().getEventos().contains(evento));
		
		//verifica se o diretor possui o evento
		Assert.assertTrue(evento.getDiretor().getEventos().contains(evento));
		
		//verifica se o artista possui o evento
		for(Artista artista : evento.getArtistas()){
			Assert.assertTrue(artista.getEventos().contains(evento));
		}
		
		//Verifica se todos os ingressos foram gerados ao criar um novo Evento
		int capacidade = 0;
		for (Setor setor : evento.getAmbiente().getSetores()) {
			capacidade += setor.getCapacidade();
		}
		Assert.assertEquals(evento.getIngressos().size(), capacidade);
		
		//Verifica se o ingresso possui o evento
		for(Ingresso ingresso : evento.getIngressos()){
			Assert.assertEquals(ingresso.getEvento(),evento);
		}
		
		
		//Alteracao dos atributos
		EventoEnum pessoaEnumTemp = EventoEnum.getEvento();
		evento.setDescricao(pessoaEnumTemp.getDescricao());
		evento.setNome(pessoaEnumTemp.getNome());
		evento.setCensura(pessoaEnumTemp.getCensura());
		evento.setDataEvento(pessoaEnumTemp.getDataEvento());
		mudarAtributosEspecificos(pessoaEnumTemp, evento);
		
		
		//Verifica se alteracao dos atributos esta sendo realizada
		Assert.assertEquals(evento.getDescricao(), pessoaEnumTemp.getDescricao());
		Assert.assertEquals(evento.getNome(), pessoaEnumTemp.getNome());
		Assert.assertEquals(evento.getCensura(), pessoaEnumTemp.getCensura());
		Assert.assertEquals(evento.getDataEvento(), pessoaEnumTemp.getDataEvento());
		switch (evento.getTipoEventoEnum()) {
		case FILME:
			Assert.assertEquals(((Filme)evento).getGenero(), pessoaEnumTemp.getGenero());
			break;
		case PECA:
			Assert.assertEquals(((Peca)evento).getCompanhia(), pessoaEnumTemp.getCompanhia());
			Assert.assertEquals(((Peca)evento).getGenero(), pessoaEnumTemp.getGenero());
			break;
		case SHOW:
			Assert.assertEquals(((Show)evento).getEstilo(), pessoaEnumTemp.getEstilo());
	}			
		
		
		
		//Volta os atributos, para ficar compativel com outros testes		
		evento.setDescricao(this.eventoEnum.getDescricao());
		evento.setNome(this.eventoEnum.getNome());
		evento.setCensura(this.eventoEnum.getCensura());
		evento.setDataEvento(this.eventoEnum.getDataEvento());
		mudarAtributosEspecificos(eventoEnum, evento);
	}
}
