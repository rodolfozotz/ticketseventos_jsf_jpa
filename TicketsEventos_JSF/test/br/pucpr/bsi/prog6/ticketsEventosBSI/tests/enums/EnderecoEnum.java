package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums;

import java.util.Random;


public enum EnderecoEnum {
	
	END_BRASILIA("DF047", 1, "Aeroporto", "Lago Sul", "Brasilia", "DF", "Brasil"),
	END_BELO_HORIZONTE("MG010", 1, "Kilometro 39", "Confins", "Belo Horizonte", "MG", "Brasil"),
	END_CURITIBA("Av. Rocha Pombo", 1, "Aeroporto", "Aguas Belas", "Sao Jose dos Pinhais", "PR", "Brasil"),
	END_SAO_PAULO("Rod. Helio Smidt", 1, "Aeroporto", "Cumbica", "Guarulhos", "SP", "Brasil"),
	END_PORTO_ALEGRE("Av. Severo Dulius", 1, "Aeroporto", "Centro", "Porto Alegre", "RS", "Brasil"),
	END_RIO_JANEIRO("Praca Sen. Salgado Filho", 1, "Aeroporto", "Centro", "Rio de Janeiro", "RJ", "Brasil");
	
	private String rua, complemento, bairro, cidade, estado, pais;
	private int numero;
	
	private EnderecoEnum(String rua, int numero, 
			String complemento, String bairro, String cidade, String estado, String pais) {
		this.rua = rua;
		this.numero = numero;
		this.complemento = complemento;
		this.bairro = bairro;
		this.cidade = cidade;
		this.estado = estado;
		this.pais = pais;
	}
	
	public String getBairro() {
		return bairro;
	}
	
	public String getCidade() {
		return cidade;
	}
	
	public String getComplemento() {
		return complemento;
	}
	
	public String getEstado() {
		return estado;
	}
	
	public int getNumero() {
		return numero;
	}
	
	public String getPais() {
		return pais;
	}
	
	public String getRua() {
		return rua;
	}
	
	public static EnderecoEnum getEndereco(){
		Random generator = new Random();
		EnderecoEnum[] array = values();
		int length = array.length;
		return array[generator.nextInt(length)];
	}
	
}
