package br.pucpr.bsi.prog6.ticketsEventosBSI.tests.laboratorioDAO.queries;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.EventoBCFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.bc.VendaBC;
import br.pucpr.bsi.prog6.ticketsEventosBSI.exception.BSIException;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.AmbienteFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Cliente;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Diretor;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Evento;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.EventoFactory;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Ingresso;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.Venda;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.enums.TipoAmbienteEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.enums.TipoCompraEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.model.enums.TipoEventoEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.EventoEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.enums.PessoaEnum;
import br.pucpr.bsi.prog6.ticketsEventosBSI.tests.util.Verificador;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * IMPORTANTE: Para rodar esses teste, limpar a base do Banco de Dados antes e rodar os testes da camada BC.
 * 
 * @author Mauda
 * 
 */

public class TesteVendaDAOQueries {
	
	///////////////////////////////////////////////////////////
	// TESTE DOS METODOS findAll E findById 
	///////////////////////////////////////////////////////////
	
	@Test
	public void testFindAlleFindById(){
		List<Venda> listaBD = VendaBC.getInstance().findAll();
		
		//Verifica se a quantidade de instancias a mesma dos testes de populate
		Assert.assertEquals(listaBD.size(), EventoEnum.values().length);
		
		for (Venda eventoFindAll : listaBD) {
			Venda eventoFindId = (Venda) VendaBC.getInstance().findById(eventoFindAll.getId());
			Verificador.verificar(eventoFindAll, eventoFindId);
		}
		
		//buscando um id inexistente
		Venda eventoIdInvalido = (Venda)EventoBCFactory.getInstance(TipoEventoEnum.FILME).findById(100000L);
		Assert.assertNull(eventoIdInvalido);
	}
	
	///////////////////////////////////////////////////////////
	// TESTES DO METODO findByFilter 
	///////////////////////////////////////////////////////////	
		
	@Test(expected = BSIException.class)
	public void testFindByFilterNulo(){
		VendaBC.getInstance().findByFilter(null);
	}	
	
	@Test
	public void testFindByFilterDataVenda(){
		Venda filter = new Venda(new Cliente(new Endereco()), new ArrayList<Ingresso>());
		List<Venda> vendaFilter = VendaBC.getInstance().findByFilter(filter);
		
		//verifica se o tamanho do filtro eh 18
		Assert.assertEquals(vendaFilter.size(), 18);
	}	
	
	@Test
	public void testFindByFilterIngressoReservados(){
		Ingresso ingressoFilter = new Ingresso(null, null, null);
		ingressoFilter.setTipoCompraEnum(TipoCompraEnum.RESERVADO);
		Venda filter = new Venda(new Cliente(new Endereco()), ingressoFilter);
		List<Venda> vendaFilter = VendaBC.getInstance().findByFilter(filter);
		
		//verifica se o tamanho do filtro eh 18
		Assert.assertEquals(vendaFilter.size(), 18);
	}	
	
	@Test
	public void testFindByFilterIngressoVendidos(){
		Ingresso ingressoFilter = new Ingresso(null, null, null);
		ingressoFilter.setTipoCompraEnum(TipoCompraEnum.VENDIDO);
		Venda filter = new Venda(new Cliente(new Endereco()), ingressoFilter);
		List<Venda> vendaFilter = VendaBC.getInstance().findByFilter(filter);
		
		//verifica se o tamanho do filtro eh 18
		Assert.assertEquals(vendaFilter.size(), 18);
	}	
	
	@Test
	public void testFindByFilterCliente(){
		for (PessoaEnum pessoaEnum: PessoaEnum.getClientes()) {
			Venda filter = new Venda(new Cliente(new Endereco()), new ArrayList<Ingresso>());
			
			filter.getCliente().setNome(pessoaEnum.getNome());
			
			List<Venda> vendaFilter = VendaBC.getInstance().findByFilter(filter);
			
			//verifica se o tamanho do filtro eh 1
			Assert.assertEquals(vendaFilter.size(), 3);
			
			//Nomes sao iguais
			Assert.assertEquals(vendaFilter.get(0).getCliente().getNome(), pessoaEnum.getNome());
			
		}
	}
	
	@Test
	public void testFindByFilterEvento(){
		for (EventoEnum eventoEnum : EventoEnum.values()) {
			Evento eventoFilter = EventoFactory.getInstance(TipoEventoEnum.getBy(eventoEnum.getTipo()), 
					AmbienteFactory.getInstance(TipoAmbienteEnum.getBy(eventoEnum.getAmbienteEnum().getTipoAmbiente()), new Endereco()),
					new ArrayList<Artista>(), new Diretor(), null);
			
			Ingresso ingressoFilter = new Ingresso(eventoFilter, null, null);
			eventoFilter.setNome(eventoEnum.getNome());
			
			Venda filter = new Venda(new Cliente(new Endereco()), ingressoFilter);
			
			List<Venda> vendaFilter = VendaBC.getInstance().findByFilter(filter);
			
			//verifica se o tamanho do filtro eh 2
			Assert.assertEquals(vendaFilter.size(), 2);
		}
	}
}
